<?php

use Illuminate\Database\Seeder;
use App\Region;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::create([
            'name' => 'Ауэзовский',
            'area_id' => 1
        ]);
        Region::create([
            'name' => 'Карасайский',
            'area_id' => 1
        ]);
    }
}
