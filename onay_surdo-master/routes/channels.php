<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use Illuminate\Support\Facades\Broadcast;

/*Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});*/

Broadcast::channel('call-private.{id}', function ($user, $id) {
    /* @var $call \App\Call */
    $call = \App\Call::find($id);
    return $call ? ((int) $user->id === (int) $call->translator_id) or ((int) $user->id === (int) $call->client_id) : false;
});


