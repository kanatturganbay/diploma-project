<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::domain('client.onaysurdo.kz')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::get('/', 'ClientController@main');
    });
});

Route::domain('app.onaysurdo.kz')->group(function () {
    Route::group([],function () {
        Route::get('/',function (){
            return view('main');
        });
    });
});

Route::domain('company.onaysurdo.kz')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::get('/', 'ClientController@main');
    });
});

Route::domain('{domain}.onaysurdo.kz')->group(function () {
    Route::get('/', 'Auth\LoginController@showLoginForm');
});



Route::get('/auth', 'AuthController@loginPage')->name('login-iin');

Route::get('/auth-company', 'AuthController@loginPageCompany')->name('login-bin');

Route::get('/about-us', 'PageController@aboutUs')->name('about-us');
Route::post('/login-request', 'AuthController@loginRequest')->name('login-request');
Route::post('/login-bin-request', 'AuthController@loginBinRequest')->name('login-bin-request');


Route::get('/client/call-webview/{token}', 'ApiController@callWebView');
Route::get('/client/call-video/{token}/{call_id}', 'ApiController@callWebVideo');
Route::middleware('auth')->group(function () {
    Route::get('/top-up', 'PageController@topUp')->name('top-up');
    Route::prefix('/client')->group(function () {
        Route::get('/', 'ClientController@main')->name('client.main');
        Route::get('/history', 'ClientController@history')->name('client.history');
    });
    Route::prefix('payment')->group(function (){
        Route::post('/new-payment','PaymentController@newPayment')->name('payment.new-payment');
        Route::post('/charge','PaymentController@charge')->name('payment.charge');
    });
    Route::get('/download-text/{call_id}', 'CallController@downloadText')->name('download-text');
    Route::prefix('/translator')->group(function () {
        Route::get('/', 'ClientController@main')->name('translator.main');
        Route::get('/history', 'ClientController@historyTranslator')->name('translator.history');
    });

    Route::prefix('/personal-area')->middleware('personal-area')->group(function () {
        Route::get('/', 'PersonalAreaController@main')->name('personal-area.main');

        Route::prefix('/manage')->group(function () {
            Route::resource('translators', 'Admin\TranslatorController');
            Route::resource('directors', 'Admin\DirectorController');
            Route::resource('companies', 'Admin\CompanyController');
            Route::resource('regions', 'Admin\RegionController');
            Route::resource('areas', 'Admin\AreaController');
            Route::prefix('/requests')->group(function () {
                Route::get('/translators', 'Admin\RequestsController@translatorRequests')->name('requests.translators');
                Route::get('/companies', 'Admin\RequestsController@companyRequests')->name('requests.companies');
                Route::get('/accept/{id}', 'Admin\RequestsController@accept')->name('requests.accept');
                Route::post('/decline/{id}', 'Admin\RequestsController@decline')->name('requests.decline');
            });
        });
    });

    Route::get('/client-call', 'CallController@clientCallPage')->name('call.client.call-page');
    Route::get('/wait-call', 'CallController@translatorWaitForCalls')->name('call.translator.wait-calls');
    Route::prefix('/call/{call_id}')->group(function () {
        Route::put('/', 'CallController@callToTranslator')->name('call.call-translator');
        Route::get('/finish-call', 'CallController@finishCall')->name('call.call-finished');
        Route::get('/start-call', 'CallController@startCall')->name('call.call-started');
        Route::get('/text/{text}', 'CallController@writeText');
        Route::get('/ping', 'CallController@pingCall');
        Route::post('/send-review','CallController@sendReview');
        Route::put('/answer', 'CallController@answerToClient')->name('call.answer-client');
        Route::get('/translator', 'CallController@translatorCall')->name('call.translator-call');
        Route::get('/client', 'CallController@clientCall')->name('call.client-call');
        Route::get('/translator-in', 'CallController@foundTranslator')->name('call.found-translator');
    });
    Route::get('/create-call/{client_id}', 'CallController@createCall')->name('call.create');
});

Route::post('/messages', function (\Illuminate\Http\Request $request) {
    \App\Events\Message::dispatch($request->input('body'));
});

Auth::routes(['reset' => true]);

Route::get('/',function (){
    return view('main');
});
/*Route::redirect('/', '/client');*/
