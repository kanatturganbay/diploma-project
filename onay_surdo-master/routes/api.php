<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/auth', 'ApiController@login');
Route::get('/get-areas', 'ApiController@getAreas');
Route::middleware('auth:api')->group(function () {
    Route::prefix('/client')->group(function () {
        Route::get('/', 'ClientController@main')->name('client.main');
        Route::post('/history', 'ApiController@history')->name('client.history');
        Route::get('/about-us', 'ApiController@aboutUs');
    });
    Route::prefix('/call/{call_id}')->group(function () {
        Route::put('/', 'CallController@callToTranslator')->name('call.call-translator');
        Route::get('/finish-call', 'CallController@finishCall')->name('call.call-finished');
        Route::get('/start-call', 'CallController@startCall')->name('call.call-started');
    });
    Route::get('/create-call/{client_id}', 'CallController@createCall')->name('call.create');
});

Route::post('/set-call-video-link', 'CallController@setVideoLink')->name('call.set-video-link');
Route::post('/call-finished', 'CallController@apiFinishCall')->name('call.api-finish-call');
Route::post('/set-video-duration', 'CallController@setVideoDuration')->name('call.set-video-duration');
