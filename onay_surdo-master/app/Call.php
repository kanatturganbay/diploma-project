<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/* @property int translator_id
 * @property int client_id
 * @property string text
 * @property string duration
 * */
class Call extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['date_localized'];

    public function translator()
    {
        return $this->belongsTo('App\User', 'translator_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id', 'id');
    }

    public function getDateLocalizedAttribute()
    {
        $date = Carbon::parse($this->created_at);
        Carbon::setLocale('ru');
        return $date->diffForHumans();
    }
}
