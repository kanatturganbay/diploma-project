<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $guarded = ['id'];

    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_region','user_id');
    }

    public function getTranslatorsAttribute()
    {
        return $this->users()->where('role', 'translator')->get();
    }

    public function delete()
    {
        $this->users()->detach();
        return parent::delete();
    }
}
