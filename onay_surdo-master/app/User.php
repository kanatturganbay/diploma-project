<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/* @property string role
 * @property string name
 * @property string email
 * @property string iin
 * @property int balans
 * */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'iin', 'role', 'patronymic', 'surname', 'phone', 'bin', 'status', 'ip_or_too', 'company_name', 'api_token', 'balans'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function translatorCalls()
    {
        return $this->hasMany('App\Call', 'translator_id')->where('translator_id', '>', 0)->where('client_id', '>', 0)->orderByDesc('created_at');
    }

    public function getCallsAmountAttribute()
    {
        return $this->calls()->count();
    }

    public function calls()
    {
        return $this->hasMany('App\Call', 'client_id')->where('translator_id', '>', 0)->where('client_id', '>', 0)->orderByDesc('created_at');
    }

    public function getDiplomAttribute()
    {
        if ($this->documents()->where('name', 'diplom')->count())
            return $this->documents()->where('name', 'diplom')->first()->link;
        return '';
    }

    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    public function getGphAttribute()
    {
        if ($this->documents()->where('name', 'gph')->count())
            return $this->documents()->where('name', 'gph')->first()->link;
        return '';
    }

    public function getRegionAttribute()
    {
        if ($this->regions()->count()) {
            return $this->regions()->first()->id;
        }
        return '';
    }

    public function references()
    {
        return $this->documents()->where('name', 'reference');
    }

    public function area()
    {
        if ($this->regions()->count()) {
            return $this->regions()->first()->area->name;
        }
        return '';
    }

    public function regions()
    {
        return $this->belongsToMany('App\Region', 'user_region');
    }

    public function director()
    {
        return $this->belongsTo('App\User', 'added_by', 'id');
    }

    public function translators()
    {
        return $this->hasMany('App\User', 'added_by', 'id');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function charges()
    {
        return $this->hasMany('App\Charge');
    }

    public function translatorReviews()
    {
        return $this->hasMany('App\Review', 'translator_id');
    }
}
