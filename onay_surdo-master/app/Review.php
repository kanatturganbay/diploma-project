<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/* @property string content
 * @property int rate
 * */
class Review extends Model
{
    protected $guarded = ['id'];

    public function call()
    {
        return $this->belongsTo('App\Call','call_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User','client_id');
    }

    public function translator()
    {
        return $this->belongsTo('App\User','translator_id');
    }
}
