<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $guarded = ['id'];

    public function regions()
    {
        return $this->hasMany('App\Region');
    }

    public function delete()
    {
        $this->regions()->delete();
        return parent::delete();
    }
}
