<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginPage()
    {

        return view('client.iin-auth');
    }

    public function loginPageCompany()
    {
        return view('client.company');
    }

    public function loginRequest(Request $request)
    {
        $request->validate([
            'iin' => 'required|numeric',
        ]);

        $user = User::where('iin', $request->iin)->where('role', 'client')->first();

        if ($user and $user->status !== 'moderating') {
            Auth::login($user);
            return response()->json('success')->setStatusCode(200);
        }
        return response()->json('not found')->setStatusCode(404);

    }

    public function loginBinRequest(Request $request)
    {
        $request->validate([
            'bin' => 'required|numeric'
        ]);

        $user = User::where('bin', $request->bin)->where('role', 'company');
        if ($request->filial) {
            $filial = intval(ltrim($request->filial,'0'));
            $user = $user->where('filial_number', $filial);
        }
        $user = $user->first();

        if ($user and $user->status !== 'moderating') {
            Auth::login($user);
            return response()->json('success')->setStatusCode(200);
        }
        return response()->json('not found')->setStatusCode(404);

    }
}
