<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notifications\Declined;
use App\User;
use Illuminate\Http\Request;

class RequestsController extends Controller
{
    public function translatorRequests()
    {
        $requests = User::where('role', 'translator')->where('status', 'moderating')->get();
        return view('personal-area.requests.translators', compact('requests'));
    }

    public function companyRequests()
    {
        $requests = User::where('role', 'company')->where('status', 'moderating')->get();
        //dd($requests);
        return view('personal-area.requests.companies', compact('requests'));
    }

    public function accept($id)
    {
        $user = User::findOrFail($id);
        $user->status = '';
        $user->save();
        return redirect()->back();
    }

    public function decline(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->status = 'declined';
        $user->decline_reason = $request->decline_reason;
        //TODO check sending decline reason to email
        $user->notify(new Declined($request->decline_reason));
        $user->save();
        $user->delete();
        return redirect()->back();
    }

}
