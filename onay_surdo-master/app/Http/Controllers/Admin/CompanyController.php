<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use App\Http\Controllers\Controller;
use App\Notifications\Credentials;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = User::with('calls')->where('role', 'company');
        $user = Auth::user();

        if ($user->role === 'director') {
            $companies = $companies->where('added_by', $user->id)->get();
        } else {
            $companies = $companies->get();
        }
        return view('personal-area.manage.company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(session('errors'));
        return view('personal-area.manage.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedRequest = $request->validate([
            'name' => 'required',
            'iin' => 'digits:12|unique:users',
            'bin' => 'digits:12|unique:users',
            'email' => 'required|email',
            'region' => 'required',
            'phone' => 'required'
        ], [
            'name.required' => 'Заполните имя',
            'iin.required' => 'Заполните имя',
            'email.required' => 'Заполните имя',
            'region.required' => 'Выберите регионы',
            'phone.required' => 'Заполните номер телефона',
            'iin.unique' => 'Этот ИИН уже занят',
            'iin.digits' => 'ИИН должен состоять из 12 цифр',
            'bin.unique' => 'Этот БИН уже занят',
            'bin.digits' => 'БИН должен состоять из 12 цифр',
            'email.unique' => 'Этот email уже занят'
        ]);
        //Generate password
        $password = Str::random(8);
        /* @var $user User */
        $user = User::create(array_merge($request->except('password'), ['password' => Hash::make($password), 'role' => 'company', 'api_token' => Str::random(60)]));
        //Saving region
        $user->regions()->sync([$request->region]);
        //if director save added by
        if (Auth::user()->role === 'director') {
            $user->added_by = Auth::id();
            $user->status = 'moderating';
        }
        //Send password to user
        //$this->dispatch(new SendEmail(new Credentials($request->email, $password), $request->email));
        $user->notify(new Credentials($request->email, $password));
        $user->save();
        if ($request->hasFile('documents')) {
            foreach ($request->documents as $document) {
                /* @var $document UploadedFile */
                //TODO splfileinfo error check
                $name = toCyrillic(str_replace(' ', '_', time() . '_' . $document->getClientOriginalName()));
                $document->move(public_path('documents'), $name);
                Document::create([
                    'user_id' => $user->id,
                    'name' => 'reference',
                    'link' => 'documents/' . $name,
                ]);
            }
        }
        return redirect()->route('companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param   $user_id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('personal-area.manage.company.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('personal-area.manage.company.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        /* @var $user User */
        $user = User::findOrFail($user_id);
        $user->update($request->all());
        $regions = explode(',', trim($request->regions, ','));
        //Saving region
        $user->regions()->sync([$request->region]);

        return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        /* @var $user User */
        $user = User::findOrFail($user_id);
        try {
            $user->delete();
        } catch (\Exception $e) {
        }
        return redirect()->route('companies.index');
    }
}
