<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use App\Http\Controllers\Controller;
use App\Notifications\Credentials;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TranslatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $translators = User::with('calls')->where('role', 'translator');
        $user = Auth::user();

        if ($user->role === 'director') {
            $translators = $translators->where('added_by', $user->id)->get();
        } else {
            $translators = $translators->get();
        }

        return view('personal-area.manage.translator.index', compact('translators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(session('errors'));
        return view('personal-area.manage.translator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedRequest = $request->validate([
            'name' => 'required',
            'iin' => 'required|digits:12|unique:users',
            'email' => 'required|email|unique:users',
            'regions' => 'required',
            'phone' => 'required'
        ], [
            'name.required' => 'Заполните имя',
            'iin.required' => 'Заполните имя',
            'email.required' => 'Заполните имя',
            'regions.required' => 'Выберите регионы',
            'phone.required' => 'Заполните номер телефона',
            'iin.unique' => 'Этот ИИН уже занят',
            'iin.digits' => 'ИИН должен состоять из 12 цифр',
            'email.unique' => 'Этот email уже занят'
        ]);
        //Generate password
        $password = Str::random(8);
        /* @var $user User */
        $user = User::create(array_merge($request->except('password'), ['password' => Hash::make($password), 'role' => 'translator', 'api_token' => Str::random(60)]));
        //if director save added by
        if (Auth::user()->role === 'director') {
            $user->added_by = Auth::id();
            $user->status = 'moderating';
        }
        //Saving regions
        $regions = explode(',', trim($request->regions, ','));
        $user->regions()->sync($regions);
        //Saving photo
        if ($request->hasFile('photo')) {
            $user->photo_url = uploadImage($request->file('photo'));
        }
        if ($request->hasFile('diplom')) {
            Document::create([
                'user_id' => $user->id,
                'name' => 'diplom',
                'link' => 'documents/' . uploadFile($request->file('diplom'), 'documents')
            ]);
        }
        if ($request->hasFile('gph')) {
            Document::create([
                'user_id' => $user->id,
                'name' => 'gph',
                'link' => 'documents/' . uploadFile($request->file('gph'), 'documents')
            ]);
        }
        //Send password to user
        //$this->dispatch(new SendEmail(new Credentials($request->email, $password), $request->email));
        $user->notify(new Credentials($request->email, $password));

        $user->save();
        return redirect()->route('translators.index');
    }

    /**
     * Display the specified resource.
     *
     * @param   $user_id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('personal-area.manage.translator.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('personal-area.manage.translator.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        /* @var $user User */
        $user = User::findOrFail($user_id);
        $user->update($request->all());
        $regions = explode(',', trim($request->regions, ','));
        $user->regions()->sync($regions);
        //Saving photo
        if ($request->hasFile('photo')) {
            $user->photo_url = uploadImage($request->file('photo'));
            $user->save();
        }
        return redirect()->route('translators.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        /* @var $user User */
        $user = User::findOrFail($user_id);
        try {
            $user->delete();
        } catch (\Exception $e) {
        }
        return redirect()->route('translators.index');
    }
}
