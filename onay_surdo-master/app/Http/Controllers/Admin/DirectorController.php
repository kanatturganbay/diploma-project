<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Notifications\Credentials;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DirectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $directors = User::with('calls')->where('role', 'director')->get();
        return view('personal-area.manage.director.index', compact('directors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd(session('errors'));
        return view('personal-area.manage.director.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedRequest = $request->validate([
            'name' => 'required',
            'iin' => 'required|digits:12|unique:users',
            'email' => 'required|email|unique:users',
            'region' => 'required',
            'phone' => 'required'
        ],[
            'name.required' => 'Заполните имя',
            'iin.required' => 'Заполните имя',
            'phone.required' => 'Заполните номер телефона',
            'email.required' => 'Заполните имя',
            'region.required' => 'Выберите регион',
            'iin.unique' => 'Этот ИИН уже занят',
            'iin.digits' => 'ИИН должен состоять из 12 цифр',
            'email.unique' => 'Этот email уже занят'
        ]);
        //Generate password
        $password = Str::random(8);
        /* @var $user User */
        $user = User::create(array_merge($request->except('password'), ['password' => Hash::make($password), 'role' => 'director', 'api_token' => Str::random(60)]));
        //Saving region
        $user->regions()->sync([$request->region]);
        //Saving photo
        if ($request->hasFile('photo')) {
            $user->photo_url = uploadImage($request->file('photo'));
        }
        //Send password to user
        //$this->dispatch(new SendEmail(new Credentials($request->email, $password), $request->email));
        $user->notify(new Credentials($request->email, $password));

        $user->save();
        return redirect()->route('directors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param   $user_id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('personal-area.manage.translator.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $user = User::findOrFail($user_id);
        return view('personal-area.manage.director.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        /* @var $user User */
        $user = User::findOrFail($user_id);
        $user->update($request->all());
        $regions = explode(',', trim($request->regions, ','));
        //Saving region
        $user->regions()->sync([$request->region]);
        //Saving photo
        if ($request->hasFile('photo')) {
            $user->photo_url = uploadImage($request->file('photo'));
            $user->save();
        }
        return redirect()->route('directors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $user_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        /* @var $user User */
        $user = User::findOrFail($user_id);
        try {
            $user->delete();
        } catch (\Exception $e) {
        }
        return redirect()->route('translators.index');
    }
}
