<?php

namespace App\Http\Controllers;

use App\Charge;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

    public function newPayment(Request $request)
    {
        $payment = new Payment([
            'user_id' => Auth::id(),
            'amount' => $request->get('amount'),
            'cloud_invoice_id' => $request->get('invoice_id'),
            'status' => 'paid'
        ]);
        $payment->save();
        /* @var $user User */
        $user = Auth::user();
        $user->balans += (int)$request->get('amount');
        $user->save();
    }

    public function charge(Request $request)
    {
        $charge = new Charge([
            'call_id' => $request->get('call_id'),
            'amount' => 10,
            'user_id' => Auth::id()
        ]);
        $charge->save();
        /* @var $user User */
        $user = Auth::user();
        $user->balans -= 10;
        if ($user->balans <= 0){
            $user->balans = 0;
            $user->save();
            return response()->json('no balans')->setStatusCode(400);
        }
        $user->save();
        return $user->balans;
    }
}
