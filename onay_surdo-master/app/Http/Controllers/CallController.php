<?php

namespace App\Http\Controllers;

use App\Call;
use App\Events\CallFinished;
use App\Events\CallNotWaiting;
use App\Events\CallReceived;
use App\Events\NewCall;
use App\Events\SentOffer;
use App\Events\TranslatorFound;
use App\Review;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CallController extends Controller
{
    public function createCall($client_id)
    {
        /* @var $user User */
        $user = User::findOrFail($client_id);
        $call = new Call([
            'client_id' => $client_id,
            'translator_id' => null,
            'duration' => 0
        ]);
        $call->save();
        NewCall::dispatch(Call::with('client')->find($call->id), $user->region);
        return response()->json($call);
    }

    public function translatorCall($call_id)
    {
        $call = Call::find($call_id);
        $call->translator_id = Auth::id();
        $call->save();
        return view('translator.video-call', compact('call'));
    }

    public function clientCall($call_id)
    {
        $call = Call::find($call_id);
        return view('client.video-call', compact('call'));
    }

    public function translatorWaitForCalls()
    {
        /* @var $user User */
        $user = Auth::user();
        $regions = $user->regions()->pluck('regions.id');
        $waitingCalls = Call::with('client')->where('status', 'waiting')->whereNull('translator_id')
            ->whereHas('client.regions', function (Builder $query) use ($regions) {
                $query->whereIn('regions.id', $regions);
            }, '>=', 1)
            ->where('last_pinged_at', '>=', Carbon::now()->subMinutes(5))
            ->get();
        return view('translator.waiting-call', compact('waitingCalls', 'regions'));
    }

    public function clientCallPage()
    {
        return view('client.video-call');
    }

    public function callToTranslator(Request $request, $call_id)
    {
        $call = Call::with('client')->find($call_id);
        CallReceived::dispatch($call, $request->type, Auth::user(), $request->data);
        return response()->json('success');
    }

    public function answerToClient(Request $request, $call_id)
    {
        $call = Call::with('client', 'translator')->find($call_id);
        SentOffer::dispatch($call, $request->type, Auth::user(), $request->data);
        return response()->json('success');
    }

    public function foundTranslator($call_id)
    {
        $call = Call::find($call_id);
        $call->translator_id = Auth::id();
        TranslatorFound::dispatch($call, Auth::user());
        return response()->json('success');
    }

    public function downloadText($call_id)
    {
        $call = Call::find($call_id);
        $path = public_path(Str::random() . "-$call_id.txt");
        $myfile = fopen($path, "w") or die("Unable to open file!");
        fwrite($myfile, $call->text);
        fclose($myfile);
        return response()->file($path);
    }

    public function writeText($call_id, $text)
    {
        /* @var $call Call */
        $call = Call::find($call_id);
        $call->text .= "\n$text";
        $call->save();
    }

    public function setVideoLink(Request $request)
    {
        /* @var $call Call */
        $call = Call::find($request->call_id);
        if ($request->is_translator === 'true') {
            $call->translator_video_link = $request->link;
        } else {
            $call->client_video_link = $request->link;
        }
        $call->save();
        return $call->id . " " . ($request->is_translator === 'true' ? 'translator' : 'client') . ' link updated successfully';
    }

    public function setVideoDuration(Request $request)
    {
        /* @var $call Call */
        $call = Call::find($request->call_id);
        $call->duration = (int)$request->duration;
        $call->save();
        return $call->id . ' video duration updated successfully';
    }

    public function finishCall($call_id)
    {
        $call = Call::find($call_id);
        $call->status = 'finished';
        $call->finished_at = Carbon::now();
        if ($call->translator_id and $call->client_id) {
            $user = Auth::user();
            $user->first_call = false;
            $user->save();
        }
        $call->save();
        CallNotWaiting::dispatch($call, $call->client->region);
        CallFinished::dispatch($call);
        if ($call->finished_at and $call->started_at) {
            $finished_time = Carbon::createFromFormat('Y-m-d H:i:s', $call->finished_at);
            $started_time = Carbon::createFromFormat('Y-m-d H:i:s', $call->started_at);
            $duration = abs($finished_time->diffInSeconds($started_time));
            if ($duration) {
                $call->duration = $duration;
            }
        }
        return 'success';
    }

    public function apiFinishCall(Request $request)
    {
        if ($request->password == "les1") {
            $call = Call::find($request->call_id);
            $call->status = 'finished';
            if ($call->translator_id and $call->client_id) {
                $user = Auth::user();
                $user->first_call = false;
                $user->save();
            }
            $call->finished_at = Carbon::now();
            $call->save();
            CallNotWaiting::dispatch($call, $call->client->region);
            CallFinished::dispatch($call);
            return 'success';
        }
        return "incorrect password";
    }

    public function startCall($call_id)
    {
        $call = Call::find($call_id);
        $call->status = 'started';
        $call->started_at = Carbon::now();
        $call->last_pinged_at = Carbon::now();
        $call->save();
        CallNotWaiting::dispatch($call, $call->client->region);
        return 'success';
    }

    public function pingCall($call_id)
    {
        $call = Call::find($call_id);
        $call->last_pinged_at = Carbon::now();
        $call->save();
        return 'success';
    }

    public function sendReview(Request $request, $call_id)
    {
        $review = new Review([
            'content' => $request->get('content'),
            'rate' => $request->get('rate'),
            'translator_id' => $request->get('translator_id'),
            'call_id' => $call_id,
            'client_id' => Auth::id()
        ]);
        $review->save();
    }
}
