<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function main()
    {
        $user = Auth::user();
        return view('client.main', compact('user'));
    }

    public function company()
    {
        $user = Auth::user();
        return view('client.company', compact('user'));
    }

    public function history()
    {
        $user = User::with('calls.translator', 'calls.client')->find(Auth::id());
        return view('client.history', compact('user'));
    }

    public function historyTranslator()
    {
        $user = User::with('translatorCalls.translator', 'translatorCalls.client')->find(Auth::id());
        return view('translator.history', compact('user'));
    }


}
