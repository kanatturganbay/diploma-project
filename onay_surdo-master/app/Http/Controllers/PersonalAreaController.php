<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalAreaController extends Controller
{
    public function main()
    {
        /* @var $user User */
        $user = Auth::user();
        if ($user->role === 'admin') {
            return view('personal-area.admin.main', compact('user'));
        }
        if ($user->role === 'director') {
            return view('personal-area.director.main', compact('user'));
        }
        if ($user->role === 'company') {
            return view('personal-area.company.main', compact('user'));
        }
        return abort(404);
    }
}
