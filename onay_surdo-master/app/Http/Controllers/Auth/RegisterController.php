<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Psy\Util\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/client';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'patronymic' => ['required', 'string', 'max:255'],
            'iin' => 'required|digits:12|unique:users',
            'regions' => 'required',
            'phone' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], [
            'name.required' => 'Заполните имя',
            'surname.required' => 'Заполните имя',
            'patronymic.required' => 'Заполните имя',
            'iin.required' => 'Заполните ИИН',
            'email.required' => 'Заполните email',
            'regions.required' => 'Выберите регионы',
            'phone.required' => 'Заполните номер телефона',
            'iin.unique' => 'Этот ИИН уже занят',
            'iin.digits' => 'ИИН должен состоять из 12 цифр',
            'email.unique' => 'Этот email уже занят',
            'password.confirmed' => 'Пароли не совпадают',
            'password.required' => 'Заполните пароль',
            'password.min' => 'Пароль должен состоять минимум из 8 символов'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['name'],
            'patronymic' => $data['patronymic'],
            'iin' => $data['iin'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => 'client',
            'api_token' => \Illuminate\Support\Str::random(60)
        ]);
        //Saving regions
        $regions = explode(',', trim($data['regions'], ','));
        $user->regions()->sync($regions);
        return $user;
    }
}
