<?php

namespace App\Http\Controllers;

use App\Area;
use App\Call;
use App\Events\CallFinished;
use App\Events\CallNotWaiting;
use App\Events\CallReceived;
use App\Events\NewCall;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public function getAreas()
    {
        return Area::with('regions')->get();
    }

    public function callWebView($token)
    {
        $user = User::where('api_token', $token)->first();
        if (!$user) {
            return abort(404);
        }
        Auth::login($user);
        return view('api.client.video-call');
    }

    public function callWebVideo($token, $call_id)
    {
        $user = User::where('api_token', $token)->first();
        $call = Call::with('client', 'translator')->find($call_id);
        if (!$user or !$call) {
            return abort(404);
        }
        Auth::login($user);
        return view('api.client.video', compact('call'));
    }

    public function login(Request $request)
    {
        $request->validate([
            'iin' => 'required',
        ]);
        $iin = $request->iin;
        if (is_string($iin)) {
            $iin = (int)$iin;
        }
        $user = User::where(function ($query) use ($iin){
            /* @var $query Builder */
            $query->where('iin',$iin)->orWhere('bin',$iin);
        })->whereIn('role', ['client','company'])->first();
        if ($user and $user->status !== 'moderating') {
            Auth::login($user);
            return response()->json($user)->setStatusCode(200);
        }
        return response()->json(false)->setStatusCode(404);
    }

    public function createCall($client_id)
    {
        $call = new Call([
            'client_id' => $client_id,
            'translator_id' => null,
            'duration' => 0
        ]);
        $call->save();
        NewCall::dispatch(Call::with('client')->find($call->id));
        return response()->json($call);
    }


    public function callToTranslator(Request $request, $call_id)
    {
        $call = Call::with('client')->find($call_id);
        CallReceived::dispatch($call, $request->type, Auth::user(), $request->data);
        return response()->json('success');
    }

    public function history(Request $request)
    {
        $calls = Call::with('translator', 'client')
            ->where('client_id', Auth::id())
            ->where('translator_id', '>', 0)
            ->where('client_id', '>', 0)
            ->orderBy('created_at', $request->order ?? 'asc')
            ->get();
        foreach ($calls as $call) {
            $call->duration = (int)($call->duration / 60) . ' мин ' . $call->duration % 60;
        }

        return response()->json($calls);
    }

    public function finishCall($call_id)
    {
        $call = Call::find($call_id);
        $call->status = 'finished';
        $call->finished_at = Carbon::now();
        $call->save();
        CallNotWaiting::dispatch($call);
        CallFinished::dispatch($call);
        if ($call->finished_at and $call->started_at) {
            $finished_time = Carbon::createFromFormat('Y-m-d H:i:s', $call->finished_at);
            $started_time = Carbon::createFromFormat('Y-m-d H:i:s', $call->started_at);
            $duration = abs($finished_time->diffInSeconds($started_time));
            if ($duration) {
                $call->duration = $duration;
            }
        }
        return 'success';
    }


    public function startCall($call_id)
    {
        $call = Call::find($call_id);
        $call->status = 'started';
        $call->started_at = Carbon::now();
        $call->save();
        CallNotWaiting::dispatch($call);
        return 'success';
    }

    public function aboutUs()
    {
        return response()->json([
            'img' => asset('logo-surdo.jpg'),
            'text' => "<div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-9\">
                    <h4>«АЙРИЗ-ҚАМҚОР» МҮГЕДЕКТЕР ҚОҒАМДЫҚ БІРЛЕСТІГІ</h4>
                    <p>Қазақстан Республикасы, Алматы облысы, Жамбыл ауданы, Самсы ауылы, Тлеукабыл көшесі, № 2 «А» үй,
                        почталық индекс 040623. e-mail: <a href=\"mailto:airizkamkor@mail.ru\">airizkamkor@mail.ru</a>, тел:
                        <a href=\"tel:8 707 354 09 99\">8 707 354 09 99; 8 777 589 29 11</a>.</p>
                    <h4>ОБЩЕСТВЕННОЕ ОБЪЕДИНЕНИЯ ИНВАЛИДОВ «АЙРИЗ-ҚАМҚОР»</h4> 
                    <p>
                        Республика Казахстан, Алматинская область, Жамбылский район, село Самсы, улица Тлеукабыл, дом № 2
                        «А»,
                        почтовый индекс 040623.
                        e-mail: <a href=\"mailto:airizkamkor@mail.ru\">airizkamkor@mail.ru</a>,
                        тел: <a href=\"tel:8 707 354 09 99\">8 707 354 09 99; 8 777 589 29 11</a>.
    
                        Миссия организации
    
                        Защита прав и интересов людей с ограниченными возможностями здоровья, содействие широкому участию их
                        в
                        общественно-политической, спортивной и культурной жизни общества, развитие физического и духовного
                        потенциала
                        «Онай-сурдо»- интеграция людей с ограниченными возможностями здоровья по слуху и речи, снятие
                        барьеров
                        для
                        трудоустройста и эфективное реализация государственных льготных програм.
                    </p>
                </div>
                </div>
        </div>"
        ]);

    }
}
