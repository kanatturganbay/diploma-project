<?php

namespace App\Http\Controllers;

use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function aboutUs()
    {
        return view('about-us');
    }

    public function topUp()
    {
        $operations = new Collection();
        /* @var $user User */
        $user = Auth::user();
        /* @var $payments Collection */
        $payments = $user->payments;
        $charges = $user->charges()->groupBy('charges.call_id')->selectRaw('max(id) as id,call_id,max(created_at) as created_at,sum(amount) as amount')
            ->get();
        $payments = $payments->merge($charges)->sortByDesc('created_at');
        return view('client.top-up',['operations' => $payments]);
    }
}
