<?php

namespace App\Http\Middleware;

use Closure;

class NodeJsServer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->token != 'poje6662n23disSDxmaiwejWwjqnqdw') {
            abort(404);
        }
        return $next($request);
    }
}
