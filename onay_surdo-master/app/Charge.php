<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $guarded = ['id'];

    public function call()
    {
        return $this->belongsTo('App\Call');
    }
}
