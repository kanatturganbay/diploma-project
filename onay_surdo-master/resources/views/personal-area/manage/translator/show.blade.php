@extends('layouts.app')

@section('content')
    <div class="container d-flex flex-column align-items-center">
        <div class="card" style="width:100%">
            @if($user->photo_url)
                <div style="width: 100%" class="d-flex justify-content-center">
                    <img class="card-img-top" src="{{asset('images/'.$user->photo_url)}}" alt="Card image"
                         style="width:50%">
                </div>
            @endif
            <div class="card-body">
                <h4 class="card-title">{{$user->surname}} {{$user->name}} {{$user->patronymic}}</h4>
                <p class="card-text">ИИН: {{$user->iin}}</p>
                <p class="card-text">Область: {{$user->area()}}</p>
                <p class="card-text">Районы куда он прикреплен: {{$user->regions()->pluck('name')->implode(',')}}</p>
                {{--<a href="#" class="btn btn-primary">See Profile</a>--}}
                <div class="row my-3">
                    <div class="col-sm-12">
                        <h4>Отзывы</h4>
                    </div>
                    @foreach($user->translatorReviews as $review)
                        <div class="card col-sm-12">
                            <div class="card-body">
                                <h4 class="card-title">{{$review->client->surname}} {{$review->client->name}} {{$review->client->patronymic}}</h4>
                                <p class="card-text"><b>Рейтинг: {{$review->rate}}</b><br>{{$review->content}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <br>
    </div>
    <div class="container flex-container history-page-container">
        <h4>История звонков</h4>
        <history mode="common"
                 :calls-prop="{{ $user->translatorCalls()->with('translator','client')->get() }}"></history>
    </div>
@endsection
