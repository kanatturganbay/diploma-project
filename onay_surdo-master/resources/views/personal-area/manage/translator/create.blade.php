@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong> {{$error}}
            </div>
        @endforeach

        <form method="post" action="{{route('translators.store')}}" enctype="multipart/form-data">
            @csrf

            <avatar-upload></avatar-upload>
            <div class="form-group">
                <label for="pwd">ИИН:</label>
                <input type="number" class="form-control" id="iin" placeholder="Введите иин" name="iin"
                       value="{{ old('iin') }}">
            </div>
            <div class="form-group">
                <label for="pwd">Фамилия:</label>
                <input type="text" class="form-control" id="surname" placeholder="Введите фамилию"
                       value="{{ old('surname') }}" name="surname">
            </div>
            <div class="form-group">
                <label for="pwd">Имя:</label>
                <input type="text" class="form-control" id="name" required placeholder="Введите имя"
                       value="{{ old('name') }}" name="name">
            </div>
            <div class="form-group">
                <label for="pwd">Отчество:</label>
                <input type="text" class="form-control" id="patronymic" placeholder="Введите отчество"
                       value="{{ old('patronymic') }}"
                       name="patronymic">
            </div>

            <select-region></select-region>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Введите email"
                       value="{{ old('email') }}" required name="email">
            </div>
            <p>Пароль автоматически сгенерируем и отправим на почту переводчика!</p>
            <div class="form-group">
                <label>Номер телефона:</label>
                <masked-input mask="\+1 (111) 111-1111" placeholder="Номер телефона" name="phone" class="form-control"
                              required="required" value="{{old('phone')}}"/>
            </div>
            @if(Auth::user()->role === 'director')
                <div class="form-group">
                    <label for="diplom">Диплом(сертификат):</label>
                    <input type="file" class="form-control-file" id="diplom" required name="diplom">
                </div>
                <div class="form-group">
                    <label for="gph">Договор ГПХ или трудовой:</label>
                    <input type="file" class="form-control-file" id="gph" required name="gph">
                </div>
            @endif


            <button type="submit" class="btn btn-primary">Создать</button>
        </form>
    </div>
@endsection
<script>

    export default {
        components: {MaskedInput}
    }
</script>