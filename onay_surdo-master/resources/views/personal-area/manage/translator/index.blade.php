@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12 d-flex">
            <h2>Переводчики</h2>
            <a href="{{route('translators.create')}}" class="ml-2 mb-2 btn btn-primary">+ Добавить</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th>ИИН</th>
                <th>Email</th>
                <th>Область</th>
                <th>Прикрепленные районы</th>
                <th>Подробнее</th>
                <th>Изменить</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($translators as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->surname}}</td>
                    <td>{{$user->patronymic}}</td>
                    <td>{{$user->iin}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->area()}}</td>
                    <td>{{$user->regions()->pluck('name')->implode(',')}}</td>
                    <td><a href="{{route('translators.show',$user->id)}}" class="btn btn-info">Подробнее</a></td>
                    <td><a href="{{route('translators.edit',$user->id)}}" class="btn btn-primary">Изменить</a></td>
                    <td>
                        <form action="{{route('translators.destroy',$user->id)}}" method="post">
                            @method('DELETE')
                            <button class="btn btn-danger">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
