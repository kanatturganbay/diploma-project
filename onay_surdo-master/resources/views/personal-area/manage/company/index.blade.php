@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12 d-flex">
            <h2>Компании</h2>
            <a href="{{route('companies.create')}}" class="ml-2 mb-2 btn btn-primary">+ Добавить</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Название компании</th>
                <th>ФИО учредителя</th>
                <th>ИП/ТОО</th>
                <th>ИИН/БИН</th>
                <th>Email</th>
                <th>Область</th>
                <th>Прикрепленный район</th>
                <th>Подробнее</th>
                <th>Изменить</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $user)
                <tr>
                    <td>{{$user->company_name}}</td>
                    <td>{{$user->name}} {{$user->surname}} {{$user->patronymic}}</td>
                    <td>{{$user->ip_or_too ? 'ИП' : 'ТОО'}}</td>
                    <td>{{$user->ip_or_too ? $user->iin : $user->bin}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->area()}}</td>
                    <td>{{$user->regions()->pluck('name')->implode(',')}}</td>
                    <td><a href="{{route('companies.show',$user->id)}}" class="btn btn-primary">Подробнее</a></td>
                    <td><a href="{{route('companies.edit',$user->id)}}" class="btn btn-primary">Изменить</a></td>
                    <td>
                        <form action="{{route('companies.destroy',$user->id)}}" method="post">
                            @method('DELETE')
                            <button class="btn btn-danger">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
