@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong> {{$error}}
            </div>
        @endforeach

        <form method="post" action="{{route('companies.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="company_name">Название компании:</label>
                <input type="text" class="form-control" id="company_name" placeholder="Название компании"
                       value="{{ old('company_name') }}" name="company_name">
            </div>
            <requisit iin-prop="{{ old('iin') }}" is-ip-prop="{{old('ip_or_too')}}"
                      bin-prop="{{ old('bin') }}"></requisit>
            <div class="form-group">
                <label for="pwd">Фамилия учредителя:</label>
                <input type="text" class="form-control" id="surname" placeholder="Введите фамилию"
                       value="{{ old('surname') }}" name="surname">
            </div>
            <div class="form-group">
                <label for="pwd">Имя учредителя:</label>
                <input type="text" class="form-control" id="name" required placeholder="Введите имя"
                       value="{{ old('name') }}" name="name">
            </div>
            <div class="form-group">
                <label for="pwd">Отчество учредителя:</label>
                <input type="text" class="form-control" id="patronymic" placeholder="Введите отчество"
                       value="{{ old('patronymic') }}"
                       name="patronymic">
            </div>
            <select-region :single="true"></select-region>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Введите email"
                       value="{{ old('email') }}" required name="email">
            </div>
            <p>Пароль автоматически сгенерируем и отправим на почту!</p>
            <div class="form-group">
                <label>Номер телефона:</label>
                <masked-input mask="\+1 (111) 111-1111" placeholder="Номер телефона" name="phone" class="form-control"
                              required="required" value="{{old('phone')}}"/>
            </div>
            <div class="form-group">
                <label for="documents">Загрузите справки о компании и об инвалиде(PDF):</label>
                <input type="file" id="documents" name="documents[]" multiple class="form-control-file"
                       accept="application/pdf">
            </div>

            <button type="submit" class="btn btn-primary">Создать</button>
        </form>
    </div>
@endsection