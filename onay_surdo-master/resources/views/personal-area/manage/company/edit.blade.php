@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong> {{$error}}
            </div>
        @endforeach

        <form method="post" action="{{route('companies.update',$user)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="company_name">Название компании:</label>
                <input type="text" class="form-control" id="company_name" placeholder="Название компании"
                       value="{{ old('company_name') ?? $user->company_name }}" name="company_name">
            </div>
            <div class="form-group">
                <label for="pwd">Фамилия учредителя:</label>
                <input type="text" class="form-control" id="surname" placeholder="Введите фамилию"
                       value="{{ old('surname') ?? $user->surname }}" name="surname">
            </div>
            <div class="form-group">
                <label for="pwd">Имя учредителя:</label>
                <input type="text" class="form-control" id="name" required placeholder="Введите имя"
                       value="{{ old('name') ?? $user->name }}" name="name">
            </div>
            <div class="form-group">
                <label for="pwd">Отчество учредителя:</label>
                <input type="text" class="form-control" id="patronymic" placeholder="Введите отчество"
                       value="{{ old('patronymic') ?? $user->patronymic }}"
                       name="patronymic">
            </div>
            <select-region :single="true" :area-prop="{{ count($user->regions) ? $user->regions[0]->area->id : ''  }}"
                           :regions-prop="{{ count($user->regions) ? json_encode($user->regions()->pluck('regions.id')->toArray()) : ''  }}"></select-region>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Введите email"
                       value="{{ old('email') ?? $user->email }}" required name="email">
            </div>
            <div class="form-group">
                <label>Номер телефона:</label>
                <masked-input mask="\+1 (111) 111-1111" placeholder="Номер телефона" name="phone" class="form-control"
                              required="required" value="{{$user->phone}}"/>
            </div>
            @if(\Illuminate\Support\Facades\Auth::user()->role === 'admin')
                <div class="form-group">
                    <label for="balans">Баланс:</label>
                    <input type="number" class="form-control" id="balans" placeholder="Баланс"
                           value="{{ old('balans') ?? $user->balans }}" name="balans">
                </div>
            @endif

            <button type="submit" class="btn btn-primary">Изменить</button>
        </form>
    </div>
@endsection
