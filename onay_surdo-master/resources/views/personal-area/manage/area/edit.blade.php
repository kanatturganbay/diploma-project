@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong> {{$error}}
            </div>
        @endforeach

        <form method="post" action="{{route('areas.update',$area)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="company_name">Название области:</label>
                <input type="text" class="form-control" id="name" required placeholder="Название"
                       value="{{ old('name') ?? $area->name }}" name="name">
            </div>

            <button type="submit" class="btn btn-primary">Изменить</button>
        </form>
    </div>
@endsection
