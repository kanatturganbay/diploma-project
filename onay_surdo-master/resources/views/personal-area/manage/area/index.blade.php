@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12 d-flex">
            <h2>Области</h2>
            <a href="{{route('areas.create')}}" class="ml-2 mb-2 btn btn-primary">+ Добавить</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Имя области</th>
                <th>Регионы</th>
                <th>Изменить</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($areas as $area)
                <tr>
                    <td>{{$area->name}}</td>
                    <td>
                        <ul>
                            @foreach($area->regions as $region)
                                <li>
                                    {{$region->name}}
                                </li>
                            @endforeach
                        </ul>
                    </td>
                    <td><a href="{{route('areas.edit',$area->id)}}" class="btn btn-primary">Изменить</a></td>
                    <td>
                        <form action="{{route('areas.destroy',$area->id)}}" method="post">
                            @method('DELETE')
                            <button class="btn btn-danger">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
