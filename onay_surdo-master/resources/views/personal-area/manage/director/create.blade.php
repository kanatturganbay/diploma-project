@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong> {{$error}}
            </div>
        @endforeach

        <form method="post" action="{{route('directors.store')}}" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label for="pwd">ИИН:</label>
                <input type="number" class="form-control" id="iin" placeholder="Введите иин" name="iin"
                       value="{{ old('iin') }}">
            </div>
            <select-region :single="true"></select-region>
            <div class="form-group">
                <label for="pwd">Фамилия:</label>
                <input type="text" class="form-control" id="surname" placeholder="Введите фамилию"
                       value="{{ old('surname') }}" name="surname">
            </div>
            <div class="form-group">
                <label for="pwd">Имя:</label>
                <input type="text" class="form-control" id="name" required placeholder="Введите имя"
                       value="{{ old('name') }}" name="name">
            </div>
            <div class="form-group">
                <label for="pwd">Отчество:</label>
                <input type="text" class="form-control" id="patronymic" placeholder="Введите отчество"
                       value="{{ old('patronymic') }}"
                       name="patronymic">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Введите email"
                       value="{{ old('email') }}" required name="email">
            </div>
            <p>Пароль автоматически сгенерируем и отправим на почту директора!</p>
            <div class="form-group">
                <label>Номер телефона:</label>
                <masked-input mask="\+1 (111) 111-1111" placeholder="Номер телефона" name="phone" class="form-control"
                              required="required" value="{{old('phone')}}"/>
            </div>


            <button type="submit" class="btn btn-primary">Создать</button>
        </form>
    </div>
@endsection