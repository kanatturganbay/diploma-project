@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong> {{$error}}
            </div>
        @endforeach

        <form method="post" action="{{route('directors.update',$user)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <select-region :single="true" :area-prop="{{ count($user->regions) ? $user->regions[0]->area->id : ''  }}"
                           :regions-prop="{{ count($user->regions) ? json_encode($user->regions()->pluck('regions.id')->toArray()) : ''  }}"></select-region>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" placeholder="Введите email"
                       value="{{ old('email') ?? $user->email }}" required name="email">
            </div>
            <div class="form-group">
                <label>Номер телефона:</label>
                <masked-input mask="\+1 (111) 111-1111" placeholder="Номер телефона" name="phone" class="form-control"
                              required="required" value="{{$user->phone}}"/>
            </div>

            <button type="submit" class="btn btn-primary">Изменить</button>
        </form>
    </div>
@endsection
