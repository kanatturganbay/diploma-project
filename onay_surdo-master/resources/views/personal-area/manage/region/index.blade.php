@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12 d-flex">
            <h2>Регионы</h2>
            <a href="{{route('regions.create')}}" class="ml-2 mb-2 btn btn-primary">+ Добавить</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Имя региона</th>
                <th>Область</th>
                <th>Переводчики</th>
                <th>Изменить</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($areas as $area)
                <tr>
                    <td>{{$area->name}}</td>
                    <td>
                        {{$area->area->name}}
                    </td>
                    <td>
                        <ul>
                            @foreach($area->translators as $translator)
                                <li>
                                    <a href="{{route('translators.show',$translator->id)}}">{{$translator->name}} {{$translator->surname}} {{$translator->iin}}</a>
                                </li>
                            @endforeach
                        </ul>

                    </td>
                    <td><a href="{{route('regions.edit',$area->id)}}" class="btn btn-primary">Изменить</a></td>
                    <td>
                        <form action="{{route('regions.destroy',$area->id)}}" method="post">
                            @method('DELETE')
                            <button class="btn btn-danger">Удалить</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
