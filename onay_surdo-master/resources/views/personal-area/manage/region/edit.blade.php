@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Ошибка!</strong> {{$error}}
            </div>
        @endforeach

        <form method="post" action="{{route('regions.update',$region)}}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="name">Название региона:</label>
                <input type="text" class="form-control" id="name" required placeholder="Название"
                       value="{{ old('name') ?? $region->name }}" name="name">
            </div>
            <div class="form-group">
                <label for="area">Область:</label>
                <select class="form-control" id="area" name="area_id">
                    @foreach($areas as $area)
                        <option @if(old('area') ? old('area') === $area->id : $region->area_id === $area->id) selected
                                @endif value="{{$area->id}}">
                            {{$area->name}}
                        </option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Изменить</button>
        </form>
    </div>
@endsection
