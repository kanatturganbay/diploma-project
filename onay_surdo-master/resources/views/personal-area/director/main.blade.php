@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Добро пожаловать, {{$user->name}}!</h3>
        @include('personal-area.parts.manage-list',['links' => [
            ['link' => route('translators.index'),'name' => 'Переводчики'],['link' => route('companies.index'),'name' => 'Компании']],
        ])
    </div>
@endsection
