<div class="list-group">
        <div class="row">
            @foreach($links as $link)
            <div class="col-xs-12 col-sm-3">
                <div class="card" style="margin-top: 20px">
                    <div class="card-body">
                        <a href="{{$link['link']}}" class="list-group-item list-group-item-action">
                            <div style="display:flex;justify-content: center">
                                <i class="fas {{$link['icon']}}" style="font-size: 80px; color: #b3e8ca"></i>
                            </div>
                            <div style="display:flex;justify-content: center; font-size: 20px">
                                {{$link['name']}}
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

</div>