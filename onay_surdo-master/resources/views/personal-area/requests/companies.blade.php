@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12 d-flex">
            <h2>Заявки на добавления компании</h2>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ИП/ТОО</th>
                <th>ИИН/БИН</th>
                <th>ФИО учредителя</th>
                <th>Область</th>
                <th>Прикрепленный район</th>
                <th>ФИО директора</th>
                <th>Email</th>
                <th>Номер</th>
                <th>Справки о компании</th>
                <th>Разрешить</th>
                <th>Отклонить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($requests as $user)
                <tr>
                    <td>{{$user->ip_or_too ? 'ИП' : 'ТОО'}}</td>
                    <td>{{$user->ip_or_too ? $user->iin : $user->bin}}</td>
                    <td>{{$user->name}} {{$user->surname}} {{$user->patronymic}}</td>
                    <td>{{$user->area()}}</td>
                    <td>{{$user->regions()->pluck('name')->implode(',')}}</td>
                    <td>{{$user->director->surname}} {{$user->director->name}} {{$user->director->patronymic}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>
                        @foreach($user->references as $reference)
                            <a href="{{asset($reference->link)}}" download>Скачать</a> <br>
                        @endforeach
                    </td>
                    <td><a href="{{route('requests.accept',$user->id)}}" class="btn btn-primary">Разрешить</a></td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#myModal{{$user->id}}">
                            Отклонить
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @foreach($requests as $user)
            @include('personal-area.admin.decline-modal',['user'=>$user])
        @endforeach
    </div>
@endsection
