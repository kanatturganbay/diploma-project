@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-12 d-flex">
            <h2>Заявки на добавления переводчиков</h2>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Область</th>
                <th>Прикрепленные район</th>
                <th>ФИО директора</th>
                <th>ФИО и ИИН переводчика</th>
                <th>Диплом(сертификат)</th>
                <th>Договор трудовой или ГПХ</th>
                <th>Разрешить</th>
                <th>Отклонить</th>
            </tr>
            </thead>
            <tbody>
            @foreach($requests as $user)
                <tr>
                    <td>{{$user->area()}}</td>
                    <td>{{$user->regions()->pluck('name')->implode(',')}}</td>
                    <td>{{$user->director->surname}} {{$user->director->name}} {{$user->director->patronymic}}</td>
                    <td>{{$user->name}} {{$user->surname}} {{$user->patronymic}} {{$user->iin}}</td>
                    <td>@if($user->diplom)<a href="{{asset($user->diplom)}}" class="btn btn-primary"
                                             download>Скачать</a>@endif
                    </td>
                    <td>@if($user->gph)<a href="{{asset($user->gph)}}" class="btn btn-primary"
                                          download>Скачать</a>@endif</td>
                    <td><a href="{{route('requests.accept',$user->id)}}" class="btn btn-primary">Разрешить</a></td>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#myModal{{$user->id}}">
                            Отклонить
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @foreach($requests as $user)
            @include('personal-area.admin.decline-modal',['user'=>$user])
        @endforeach
    </div>
@endsection
