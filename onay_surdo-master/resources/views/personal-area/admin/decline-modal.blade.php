<!-- The Modal -->
<div class="modal" id="myModal{{$user->id}}">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Причина отказа</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="{{route('requests.decline',$user->id)}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="reason">Причина отказа:</label>
                                    <textarea required name="decline_reason" id="reason" cols="30" rows="10"
                                              class="form-control"></textarea>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Отправить">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>