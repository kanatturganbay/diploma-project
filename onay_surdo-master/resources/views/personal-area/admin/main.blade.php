@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Добро пожаловать, {{$user->name}}!</h3>
        @include('personal-area.parts.manage-list',['links' => [
            ['link' => route('translators.index'),'name' => 'Переводчики', 'icon' => 'fa-sign-language'],
            ['link' => route('companies.index'),'name' => 'Компании', 'icon' => 'fa-building'],
            ['link' => route('directors.index'),'name' => 'Директоры', 'icon' => 'fa-user-tie'],
            ['link' => route('areas.index'),'name' => 'Области', 'icon' => 'fa-globe-asia'],
            ['link' => route('regions.index'),'name' => 'Регионы', 'icon' => 'fa-globe-asia'],
            ['link' => route('requests.translators'),'name' => 'Заявки(Переводчики)', 'icon' => 'fa-envelope-open'],
            ['link' => route('requests.companies'),'name' => 'Заявки(Компании)', 'icon' => 'fa-file']
        ]])
    </div>
@endsection
