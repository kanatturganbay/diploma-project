<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Onaysurdo.kz</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <meta name="description"
          content="onaysurdo.kz - Видеозвонок для глухонемых,Переводчики для глухонемых,Сурдо-переводчики для глухонемых,онлайн-помощь для глухонемых.">
    <meta name="keywords"
          content="onaysurdo.kz,Видеозвонок,Видеозвонок для глухонемых,Переводчики для глухонемых,Сурдо-переводчики для глухонемых,онлайн-помощь для глухонемых">


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/9ea330dec7.js" crossorigin="anonymous"></script>
    <script src="//code.jivosite.com/widget/T9DDy2MPVL" async></script>
    <script
            src="https://code.jquery.com/jquery-3.5.0.min.js"
            integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
            crossorigin="anonymous"></script>

    <style>
        body, div, dl, dt, dd, ul, li, h1, h2, h3, h4, h5, h6, pre, code, form, fieldset, input, textarea, p, blockquote, th, td {
            margin: 0;
            padding: 0;
        }

        img {
            max-width: 100%;
            height: auto;
        }

        .logo img {
            width: 11.94444444rem;
        }

        #carousel{
            position: absolute;
            width: 100%;
            top: 0;
            z-index: -1000;
        }

        @media (max-width: 1024px) {
            #carousel {
                display: none;
            }

        }

        .container {
            width: 1400px;
            max-width: 95%;
            margin: 0 auto;
        }

        .header_main_desk {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 3.33333333rem;
        }

        .slider {
            background-color: #eeeeee;
            padding-top: 5.2rem;
            min-height: 55rem;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            position: relative;
            overflow: hidden;
        }

        .borderbox {
            z-index: 10;
        }

        .slider_krg {
            position: absolute;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 2;
            top: 0;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .slider_krg_con {
            position: relative;
            -webkit-box-flex: 2;
            -ms-flex-positive: 2;
            flex-grow: 2;
            width: 100%;
            margin: auto;
            margin-right: 0rem;
        }

        .slider_krg_con:before {
            width: 100%;
            content: '';
            background-image: url({{asset('public/media/client/images/kruga.png')}});
            position: absolute;
            left: 0;
            margin-top: 0;
            width: 100%;
            height: calc(100% - 5rem);
            background-repeat: no-repeat;
            top: 0;
            background-position: top center;
        }

        .borderbox {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .slItem {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .slItem_box {
            width: 700px;
            width: 38.88888889rem;
            -ms-flex-item-align: center;
            align-self: center;
            position: relative;
        }

        .slItem_text {
            margin-top: -8rem;
            position: relative;
            z-index: 2;
        }

        .text {
            font-family: 'Open Sans', sans-serif;
        }

        .text-aero {
            font-family: 'Aero Matics';
        }

        .text-s80 {
            font-size: 4.44444444rem;
        }

        .text-s24 {
            font-size: 1.33333333rem;
        }

        .slItem_text .text-aero {
            line-height: 118%;
            color: #2e63bb;
        }

        .slItem_after {
            margin-top: 3.1rem;
            width: 75%;
            color: #3d3d3d;
        }

        .slItem_after .text {
            line-height: 160%;
            letter-spacing: 0.8px;
            letter-spacing: 0.04444444rem;
        }

        .slItem_img {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            width: 796px;
            width: 44.22222222rem;
        }

        .slItem_box {
            z-index: 10;
        }

        .slItem_img:after {
            width: 100%;
            content: '';
            position: relative;
            left: -2rem;
            margin-bottom: 5rem;
            background-size: cover;
            margin-top: 0.5rem;
        }

        @font-face {
            font-family: 'Aero Matics';
            src: url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBoldItalic.eot')}});
            src: local('Aero Matics Bold Italic'), local('AeroMaticsBoldItalic'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBoldItalic.eot?#iefix')}}) format('embedded-opentype'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBoldItalic.woff2')}}) format('woff2'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBoldItalic.woff')}}) format('woff'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBoldItalic.ttf')}}) format('truetype');
            font-weight: bold;
            font-style: italic;
        }

        @font-face {
            font-family: 'Aero Matics';
            src: url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBold.eot')}});
            src: local('Aero Matics Bold'), local('AeroMaticsBold'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBold.eot?#iefix')}}) format('embedded-opentype'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBold.woff2')}}) format('woff2'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBold.woff')}}) format('woff'),
            url({{asset('public/media/client/fonts/Aer_Matics/AeroMaticsBold.ttf')}}) format('truetype');
            font-weight: bold;
            font-style: normal;
        }

        .nav-link {
            font-size: 20px;
        }

        .slider {
            padding-top: 0;
        }

        .lds-roller {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }

        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 40px 40px;
        }

        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 7px;
            height: 7px;
            border-radius: 50%;
            background: #fff;
            margin: -4px 0 0 -4px;
        }

        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }

        .lds-roller div:nth-child(1):after {
            top: 63px;
            left: 63px;
        }

        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }

        .lds-roller div:nth-child(2):after {
            top: 68px;
            left: 56px;
        }

        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }

        .lds-roller div:nth-child(3):after {
            top: 71px;
            left: 48px;
        }

        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }

        .lds-roller div:nth-child(4):after {
            top: 72px;
            left: 40px;
        }

        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }

        .lds-roller div:nth-child(5):after {
            top: 71px;
            left: 32px;
        }

        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }

        .lds-roller div:nth-child(6):after {
            top: 68px;
            left: 24px;
        }

        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }

        .lds-roller div:nth-child(7):after {
            top: 63px;
            left: 17px;
        }

        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }

        .lds-roller div:nth-child(8):after {
            top: 56px;
            left: 12px;
        }

        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }


    </style>
    @yield('scripts')

</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">

            <a class="navbar-brand" href="{{ url('/') }}">
                <div class="logo logo-head">
                    <img src="{{asset('public/media/client/images/logo.png')}}" alt="">
                </div>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            @guest
                <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('https://translator.onaysurdo.kz')}}">Для переводчиков</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('client.main')}}">Для клиентов</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('https://company.onaysurdo.kz/auth-company')}}">Для компаний</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('https://director.onaysurdo.kz')}}">Для директоров</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('about-us')}}">О нас</a>
                        </li>
                    </ul>
                @endguest
                @auth
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('client.main')}}">Кабинет</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('about-us')}}">О нас</a>
                        </li>
                        @if (Auth::check() and Auth::user()->role === 'company')
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('top-up')}}">Пополнить баланс</a>
                            </li>
                        @endif
                    </ul>
            @endauth

            <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    @guest
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">Регистрация для клиентов</a>
                            </li>
                        @endif
                    @endguest
                <!-- Authentication Links -->
                    @auth
                        @if (Auth::check() and Auth::user()->role === 'company')
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('top-up') }}">Баланс: <span
                                            id="balance-count">{{Auth::user()->balans}}</span></a>
                            </li>
                        @endif
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
    <main>
        <div>
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="{{asset('209813_or.jpg')}}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('jfyB6xQzEHs.jpg')}}"  alt="Second slide">
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
    </main>
    @if(Auth::user() and Auth::user()->role === 'translator')
        <new-client-alert :regions="{{Auth::user()->regions()->pluck('regions.id')}}"></new-client-alert>
    @endif
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.carousel').carousel({interval: 3000, pause: false});
        let tokens = window.location.href;
        if (tokens === 'https://app.onaysurdo.kz/'){
            $('.carousel').css('display','none');
        }
    });

</script>

</body>
</html>
