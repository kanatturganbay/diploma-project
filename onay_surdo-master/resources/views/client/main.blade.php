@extends('layouts.new')
@section('scripts')
    <style>
        .big-title {
            margin-top: -100px;
        }
        @if(in_array($user->role,['company']) and $user->first_call)
            .main-page-container .main-buttons {
                width: 52vw;
            }
        @endif
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="container flex-container main-page-container full-height">
            <h2 class="big-title"> Привет, {{$user->name}}! </h2>
            <div class="main-buttons">
                @if(!$user->role or in_array($user->role,['client','company']))
                    <a href="{{route('call.client.call-page')}}" class="button">Заказать звонок</a>
                @elseif($user->role === 'translator')
                    <a href="{{route('call.translator.wait-calls')}}" class="button">Ожидающие клиенты</a>
                @endif
                @if(in_array($user->role,['client','translator','company']))
                    <a href="{{ $user->role === 'client' ?  route('client.history') : route('translator.history')}}"
                       class="button">История вызывов</a>
                @endif
                @if(in_array($user->role,['admin','director']))
                    <a href="{{ route('personal-area.main')}}"
                       class="button">Перейти в панель управления</a>
                @endif
                @if(in_array($user->role,['company']) and $user->first_call)
                    <a href="{{route('call.client.call-page')}}"
                       class="button first-time">Заказать тестовый звонок</a>
                @endif
            </div>
        </div>
    </div>
@endsection
