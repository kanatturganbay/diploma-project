@extends('layouts.new')
@section('scripts')
    <script src="https://widget.cloudpayments.kz/bundles/cloudpayments"></script>
@endsection
@section('content')
    <div class="container">
        <top-up></top-up>
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3 class="mb-1">Операции по счету</h3>
            </div>
            <div class="col-sm-12">
                <table class="table">
                    <thead>
                    <tr>
                        <td>ID операции</td>
                        <th>Операция</th>
                        <th>Сумма</th>
                        <th>Время</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($operations as $operation)
                        @if($operation instanceof \App\Payment)
                            <tr class="table-success">
                                <td>{{$operation->id}}</td>
                                <td>Пополнение средств</td>
                                <td>{{$operation->amount}}</td>
                                <td>{{$operation->created_at}}</td>
                            </tr>
                        @elseif($operation instanceof \App\Charge)
                            <tr class="table-danger">
                                <td>{{$operation->id}}</td>
                                <td>Расход средств на звонок</td>
                                <td>-{{$operation->amount}}</td>
                                <td>{{$operation->created_at}}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
