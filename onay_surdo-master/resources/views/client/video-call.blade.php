@extends('layouts.new')

@section('content')
    <client-video-chat :user-prop="{{Auth::user()}}"
                       :first-call="{{(Auth::user()->first_call and Auth::user()->role == 'company') ? 1 : 0}}"></client-video-chat>
@endsection
