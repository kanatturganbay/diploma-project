@extends('layouts.new')

@section('content')
    <div class="container">
        <translator-video-chat :user-prop="{{Auth::user()}}" :call="{{$call}}"></translator-video-chat>
    </div>
@endsection
