@extends('layouts.new')
@section('content')
    <div class="container-fluid">
        <div class="container flex-container history-page-container">

            <h2 class="big-title"><span style="font-weight: lighter">История вызова.</span> {{$user->name}} </h2>
            <h4>ИИН: {{$user->iin}}</h4>

            <history mode="translator" :calls-prop="{{ $user->translatorCalls }}"></history>

        </div>
    </div>
@endsection