@extends('layouts.new')

@section('content')
    <div class="container">
        <h2>Ждущие клиенты</h2>
        <waiting-clients :calls-prop="{{$waitingCalls}}" :regions="{{$regions}}"></waiting-clients>
    </div>
@endsection
