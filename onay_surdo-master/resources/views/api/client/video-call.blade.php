@extends('layouts.api')

@section('content')
    <web-view-video-chat :user-prop="{{Auth::user()}}"></web-view-video-chat>
@endsection
