@extends('layouts.api')

@section('scripts')
    <link rel="stylesheet" href="https://cdn.plyr.io/3.5.6/plyr.css" />
@endsection
@section('content')
    <web-view-video :call="{{$call}}"></web-view-video>
@endsection
