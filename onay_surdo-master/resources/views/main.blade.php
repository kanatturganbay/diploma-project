@extends('layouts.new')
@section('content')
    <div class="slider" id="slider">
        <div class="slider_krg">
            <div class="slider_krg_con ">

            </div>
        </div>
        <div class="container borderbox">
            <div class="slItem">
                <div class="slItem_box">
                    <div class="slItem_text">
                        <p class="text-aero text-s80">
                            Onay Surdo.
                            Активность для всех
                        </p>
                    </div>
                    <div class="slItem_after">
                        <p class="text text-s24">
                            Мобильное и веб приложения для получение
                            услуг сурдопереводчика онлайн.
                            Мы добавляем мобильность и спонтанность
                            в каждодневные действия.
                        </p>
                    </div>
                </div>
                <div class="slItem_img">

                </div>
            </div>
        </div>
    </div>
@endsection