package thousand.group.onaysurdo.views.main.presenters.about

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import thousand.group.onaysurdo.global.presentation.BasePresenter
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.views.main.interactors.AboutInteractor

@InjectViewState
class AboutPresenter(
    override val context: Context,
    private val resourceManager: ResourceManager,
    private val interactor: AboutInteractor
) : BasePresenter<AboutView>() {

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun onStart() {
    }

    override fun onFinish() {
    }

}