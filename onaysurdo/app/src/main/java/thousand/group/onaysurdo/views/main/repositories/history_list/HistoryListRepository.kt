package thousand.group.onaysurdo.views.main.repositories.history_list

import io.reactivex.Single
import retrofit2.Response
import thousand.group.onaysurdo.models.global.HistoryModel

interface HistoryListRepository {
    fun getHistoryList(order: String): Single<Response<MutableList<HistoryModel>>>
}