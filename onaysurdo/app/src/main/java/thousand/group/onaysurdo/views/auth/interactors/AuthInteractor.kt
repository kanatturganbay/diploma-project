package thousand.group.onaysurdo.views.auth.interactors

import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.SchedulersProvider
import thousand.group.onaysurdo.views.auth.repositories.activity.AuthRepository

class AuthInteractor(
    private val repository: AuthRepository,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) {
    fun isUserSaved(): Boolean = storage.isUserSaved()
}