package thousand.group.onaysurdo.views.main.presenters.history_list

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_history_list.*
import kotlinx.android.synthetic.main.toolbar_title.*
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.adapters.HistoryAdapter
import thousand.group.onaysurdo.global.base.BaseFragment
import thousand.group.onaysurdo.global.constants.scopes.MainScope
import thousand.group.onaysurdo.global.decorations.VerticalListItemDecoration
import thousand.group.onaysurdo.global.extentions.replaceFragmentWithBackStack
import thousand.group.onaysurdo.global.extentions.visible
import thousand.group.onaysurdo.global.helpers.MainFragmentHelper
import thousand.group.onaysurdo.models.global.HistoryModel
import thousand.group.onaysurdo.views.main.presenters.history_detail.HistoryDetailFragment

class HistoryListFragment : BaseFragment(), HistoryListView {
    override val layoutRes = R.layout.fragment_history_list

    companion object {

        const val TAG = "HistoryListFragment"
        var NAV_TAG = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = TAG,
                statusBarColorRes = R.color.colorBlueFauxDenim,
                isShowBottomNavBar = true,
                posBottomNav = 1
            )
        )

        fun newInstance(): HistoryListFragment {
            return HistoryListFragment()
        }
    }

    @InjectPresenter
    lateinit var presenter: HistoryListPresenter

    @ProvidePresenter
    fun providePresenter(): HistoryListPresenter {
        val scope = getKoin().getOrCreateScope(
            MainScope.HISTORY_LIST_SCOPE,
            named(MainScope.HISTORY_LIST_SCOPE)
        )
        return scope.get {
            parametersOf(requireActivity())
        }
    }

    private lateinit var adapter: HistoryAdapter

    override fun initBundle(arguments: Bundle?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initController() {
        swipe_refresh.setOnRefreshListener {
            presenter.refresh()
        }

        order_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            presenter.spinnerCheckedListener(isChecked)
        }
    }

    override fun onDestroy() {
        getKoin().getScopeOrNull(MainScope.HISTORY_LIST_SCOPE)?.close()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        presenter.updateOnResume()
    }

    override fun setTitle(text: Int) = title_main.setText(text)

    override fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = HistoryAdapter(
                requireContext(),
                { position, model ->
                    presenter.openDetailFragment(model)
                },
                { show ->
                    showEmptyText(show)
                }
            )
        }

        orders_list.adapter = adapter
    }

    override fun setList(list: MutableList<HistoryModel>) {
        if (::adapter.isInitialized) {
            adapter.setData(list)
        }
    }

    override fun setDecor(spaceDim: Int) {
        if (orders_list.itemDecorationCount > 0) {
            orders_list.removeItemDecorationAt(0)
        }
        orders_list.addItemDecoration(
            VerticalListItemDecoration(
                spaceDim,
                adapter.itemCount
            )
        )
    }

    override fun showSwipe(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    override fun showEmptyText(show: Boolean) {
        orders_list.visible(!show)
        empty_text.visible(show)
    }

    override fun openDetailModel(model: HistoryModel) {
        activity?.supportFragmentManager?.replaceFragmentWithBackStack(
            R.id.fragment_container,
            HistoryDetailFragment.newInstance(model),
            HistoryDetailFragment.NAV_TAG
        )
    }

}