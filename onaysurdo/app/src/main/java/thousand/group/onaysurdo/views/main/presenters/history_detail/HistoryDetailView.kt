package thousand.group.onaysurdo.views.main.presenters.history_detail

import android.net.Uri
import androidx.annotation.StringRes
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import thousand.group.onaysurdo.global.base.BaseMvpView

@StateStrategyType(OneExecutionStateStrategy::class)
interface HistoryDetailView : BaseMvpView {
    fun setTitle(@StringRes text: Int)

    fun setTransVideo(transUri: Uri)

    fun setClientVideo(clientUri: Uri)

    fun setVideoDur(text: String)

    fun setVideoStartDate(text: String)

    fun setVideoEndDate(text: String)

    fun setTransFullName(text: String)

    fun setTransPhoneNumber(text: String)

    fun setClientFullName(text: String)

    fun setClientPhoneNumber(text: String)

    fun showSwipe(show: Boolean)

    fun showTransLayout(show: Boolean)

    fun showClientLayout(show: Boolean)
}