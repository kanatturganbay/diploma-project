package thousand.group.onaysurdo.views.auth.interactors

import io.reactivex.Single
import retrofit2.Response
import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.SchedulersProvider
import thousand.group.onaysurdo.models.global.User
import thousand.group.onaysurdo.views.auth.repositories.login.LoginRepository

class LoginInteractor(
    private val repository: LoginRepository,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) {
    fun auth(iin: String): Single<Response<User>> =
        repository
            .auth(iin)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())

    fun saveUser(user: User) = storage.saveUserModel(user)

    fun saveUserAccess(access: Boolean) = storage.saveUser(access)

    fun saveAccessToken(token: String) = storage.setAccessToken(token)
}