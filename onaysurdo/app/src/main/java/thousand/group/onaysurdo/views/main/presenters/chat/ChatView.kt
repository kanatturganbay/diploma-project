package thousand.group.onaysurdo.views.main.presenters.chat

import android.content.Intent
import androidx.annotation.StringRes
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import thousand.group.onaysurdo.global.base.BaseMvpView

@StateStrategyType(OneExecutionStateStrategy::class)
interface ChatView : BaseMvpView {

    fun setTitle(@StringRes text: Int)

    fun setWebViewParams()

    fun loadWebData(url: String)

    fun openSettings(intent: Intent)

    fun showWebView(show: Boolean)

    fun showUseChatBtn(show: Boolean)
}