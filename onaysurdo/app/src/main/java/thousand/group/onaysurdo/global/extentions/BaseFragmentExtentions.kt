package thousand.group.onaysurdo.global.extentions

import android.content.Context
import thousand.group.onaysurdo.global.base.BaseFragment

val BaseFragment.appContext: Context get() = activity?.applicationContext!!

fun BaseFragment.close() = fragmentManager?.popBackStack()