package thousand.group.onaysurdo.views.main.presenters.history_list

import androidx.annotation.StringRes
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import thousand.group.onaysurdo.global.base.BaseMvpView
import thousand.group.onaysurdo.models.global.HistoryModel

@StateStrategyType(OneExecutionStateStrategy::class)
interface HistoryListView : BaseMvpView {

    fun setTitle(@StringRes text: Int)

    fun setAdapter()

    fun setList(list: MutableList<HistoryModel>)

    fun setDecor(spaceDim: Int)

    fun showSwipe(show: Boolean)

    fun showEmptyText(show: Boolean)

    fun openDetailModel(model: HistoryModel)
}