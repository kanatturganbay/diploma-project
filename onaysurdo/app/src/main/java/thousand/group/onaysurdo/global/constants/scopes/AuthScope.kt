package thousand.group.onaysurdo.global.constants.scopes

object AuthScope {
    internal const val AUTH_ACTIVITY_SCOPE = "AUTH_ACTIVITY_SCOPE"
    internal const val SPLASH_SCOPE = "SPLASH_SCOPE"
    internal const val LOGIN_SCOPE = "LOGIN_SCOPE"
    internal const val REGISTER_SCOPE = "REGISTER_SCOPE"
}