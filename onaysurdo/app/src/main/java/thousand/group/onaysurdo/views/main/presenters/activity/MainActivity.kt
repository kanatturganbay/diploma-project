package thousand.group.onaysurdo.views.main.presenters.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.base.BaseActivity
import thousand.group.onaysurdo.global.constants.scopes.MainScope
import thousand.group.onaysurdo.global.extentions.clearAndReplaceFragment
import thousand.group.onaysurdo.global.extentions.setChecked
import thousand.group.onaysurdo.global.extentions.uncheckAllItems
import thousand.group.onaysurdo.global.extentions.visible
import thousand.group.onaysurdo.global.helpers.MainFragmentHelper
import thousand.group.onaysurdo.views.auth.presenters.activity.AuthActivity
import thousand.group.onaysurdo.views.main.presenters.chat.ChatFragment
import thousand.group.onaysurdo.views.main.presenters.history_list.HistoryListFragment

class MainActivity : BaseActivity(), MainView {
    override val layoutRes = R.layout.activity_main

    companion object {
        const val TAG = "MainActivity"
    }

    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter {
        val scope = getKoin().getOrCreateScope(
            MainScope.MAIN_SCOPE,
            named(MainScope.MAIN_SCOPE)
        )
        return scope.get {
            parametersOf(this)
        }
    }

    override fun initBundle(intent: Intent?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initController() {

        supportFragmentManager.registerFragmentLifecycleCallbacks(
            object : FragmentManager.FragmentLifecycleCallbacks() {

                override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {
                    super.onFragmentStarted(fm, f)

                    f.tag?.apply {
                        Log.i(TAG, this)
                        fragmentLifeCycleController(this)
                    }
                }

                override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
                    super.onFragmentPaused(fm, f)

                    if (!fm.fragments.isNullOrEmpty()) {
                        val tag = fm.fragments[fm.fragments.size - 1].tag

                        tag?.apply {
                            Log.i(TAG, this)
                            fragmentLifeCycleController(this)
                        }
                    }
                }
            },
            true
        )

        bottomNavMain.setOnNavigationItemSelectedListener {
            supportFragmentManager.apply {

                val fragment: Fragment?

                if (!this.fragments.isNullOrEmpty()) {
                    fragment = fragments[fragments.size - 1]
                } else {
                    fragment = null
                }

                when (it.itemId) {
                    R.id.navigation_chat -> {
                        if (fragment !is ChatFragment) {
                            openChatFragment()
                        }
                    }

                    R.id.navigation_history -> {
                        if (fragment !is HistoryListFragment) {
                            openHistoryListFragment()
                        }
                    }
                    R.id.navigation_about -> {
//                        if (fragment !is HomeFragment) {
//                            supportFragmentManager.clearAndReplaceFragment(
//                                R.id.fragment_container,
//                                Home.newInstance(),
//                                HomeFragment.NAV_TAG
//                            )
//                        }
                    }
                    R.id.navigation_exit -> {
                        presenter.signOut()
                    }
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun onDestroy() {
        getKoin().getScopeOrNull(MainScope.MAIN_SCOPE)?.close()
        super.onDestroy()
    }

    private fun fragmentLifeCycleController(tag: String) {

        val bottomNavVisiblityFlag = MainFragmentHelper.isShowBottomNavBar(tag)
        val bottomNavPosFlag = MainFragmentHelper.getBottomNavPos(tag)
        val statusBarFlag = MainFragmentHelper.getStatusBarColorRes(tag)

        bottomNavMain.visible(bottomNavVisiblityFlag)

        if (bottomNavPosFlag != null) {
            bottomNavMain.setChecked(bottomNavPosFlag)
        } else {
            bottomNavMain.uncheckAllItems()
        }

        if (statusBarFlag != null) {
            changeStatusBarColor(statusBarFlag)
        } else {
            changeStatusBarColor(R.color.colorAccent)
        }
    }

    override fun openChatFragment() {
        supportFragmentManager.clearAndReplaceFragment(
            R.id.fragment_container,
            ChatFragment.newInstance(),
            ChatFragment.NAV_TAG
        )
    }

    override fun openHistoryListFragment() {
        supportFragmentManager.clearAndReplaceFragment(
            R.id.fragment_container,
            HistoryListFragment.newInstance(),
            HistoryListFragment.NAV_TAG
        )
    }

    override fun openAuthActivity() {
        startActivity(Intent(this, AuthActivity::class.java))
        finish()
    }

}
