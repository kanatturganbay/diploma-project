package thousand.group.onaysurdo.global.extentions


fun Double.roundTo(): Double {
    return String.format("%.2f", this).replace(",", ".").toDouble()
}


