package thousand.group.onaysurdo.views.auth.repositories.login

import io.reactivex.Single
import retrofit2.Response
import thousand.group.onaysurdo.global.services.ServerService
import thousand.group.onaysurdo.models.global.User

class LoginRepositoryImpl(
    private val service: ServerService
) : LoginRepository {
    override fun auth(iin: String): Single<Response<User>> = service.auth(iin)


}