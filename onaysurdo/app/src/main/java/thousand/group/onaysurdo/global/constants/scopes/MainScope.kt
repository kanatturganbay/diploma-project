package thousand.group.onaysurdo.global.constants.scopes

object MainScope {
    internal const val MAIN_SCOPE = "MAIN_SCOPE"
    internal const val CHAT_SCOPE = "CHAT_SCOPE"
    internal const val HISTORY_LIST_SCOPE = "HISTORY_LIST_SCOPE"
    internal const val HISTORY_DETAIL_SCOPE = "HISTORY_DETAIL_SCOPE"
    internal const val ABOUT_SCOPE = "ABOUT_SCOPE"
}
