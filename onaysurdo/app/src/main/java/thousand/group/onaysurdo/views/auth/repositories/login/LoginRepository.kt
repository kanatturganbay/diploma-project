package thousand.group.onaysurdo.views.auth.repositories.login

import io.reactivex.Single
import retrofit2.Response
import thousand.group.onaysurdo.models.global.User

interface LoginRepository {
    fun auth(iin: String): Single<Response<User>>
}