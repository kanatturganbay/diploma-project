package thousand.group.onaysurdo.models.global

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    var id: Int,
    var name: String,
    var iin: Number,
    var phone: String?,
    var surname: String?,
    var patronymic: String?,
    var email: String?,
    var email_verified_at: String?,
    var api_token: String,
    var created_at: String,
    var updated_at: String,
    var role: String,
    var deleted_at: String?,
    var photo_url: String?,
    var ip_or_too: Int,
    var bin: String?,
    var status: String?,
    var company_name: String?,
    var added_by: String?,
    var decline_reason: String?,
    var balans: Int,
    var first_call: Int
) : Parcelable