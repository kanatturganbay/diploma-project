package thousand.group.onaysurdo.views.main.presenters.chat

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.webkit.ConsoleMessage
import android.webkit.WebChromeClient
import android.webkit.WebView
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.toolbar_title_backspace_white.*
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import permissions.dispatcher.*
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.base.BaseFragment
import thousand.group.onaysurdo.global.constants.scopes.MainScope
import thousand.group.onaysurdo.global.extentions.visible
import thousand.group.onaysurdo.global.helpers.MainFragmentHelper
import java.util.*
import android.webkit.PermissionRequest as WebPerm

@RuntimePermissions
class ChatFragment : BaseFragment(), ChatView {
    override val layoutRes = R.layout.fragment_chat

    companion object {

        const val TAG = "ChatFragment"
        var NAV_TAG = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = TAG,
                statusBarColorRes = R.color.colorBlueFauxDenim,
                isShowBottomNavBar = true,
                posBottomNav = 0
            )
        )

        fun newInstance(): ChatFragment {
            return ChatFragment()
        }
    }

    @InjectPresenter
    lateinit var presenter: ChatPresenter

    @ProvidePresenter
    fun providePresenter(): ChatPresenter {
        val scope = getKoin().getOrCreateScope(
            MainScope.CHAT_SCOPE,
            named(MainScope.CHAT_SCOPE)
        )
        return scope.get {
            parametersOf(requireActivity())
        }
    }

    private val client = object : WebChromeClient() {
        override fun onPermissionRequest(request: WebPerm?) {
            activity?.runOnUiThread {
                Log.i(TAG, "PermissionRequest = " + Arrays.toString(request?.resources))
                request?.grant(request.resources)
            }
        }

        override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
            Log.i(TAG, consoleMessage?.message())
            return super.onConsoleMessage(consoleMessage)
        }
    }

    override fun initBundle(arguments: Bundle?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initController() {
        use_chat.setOnClickListener {
            takeWebViewPermWithPermissionCheck()
        }
    }

    override fun onDestroy() {
        webView?.destroy()
        getKoin().getScopeOrNull(MainScope.CHAT_SCOPE)?.close()
        super.onDestroy()
    }

    override fun onPause() {
        webView.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        webView.onResume()
    }

    override fun setTitle(text: Int) = title_main.setText(text)


    override fun openSettings(intent: Intent) {
        activity?.startActivity(intent)
    }

    override fun showWebView(show: Boolean) = webView.visible(show)

    override fun showUseChatBtn(show: Boolean) = use_chat.visible(show)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun setWebViewParams() {
        val settings = webView.settings
        settings.javaScriptEnabled = true
        settings.mediaPlaybackRequiresUserGesture = false
        WebView.setWebContentsDebuggingEnabled(true)
        webView.webChromeClient = client
    }

    override fun loadWebData(url: String) {
        webView.loadUrl(url)
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.MODIFY_AUDIO_SETTINGS
    )
    fun takeWebViewPerm() {
        Log.i(TAG, "takePhotoPerm")
        presenter.turnOnChat()
    }

    @OnShowRationale(

        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.MODIFY_AUDIO_SETTINGS
    )
    fun showWebViewPerm(request: PermissionRequest) {
        Log.i(TAG, "showRatTakePhotoPerm")

        context?.let {
            val alert = AlertDialog.Builder(it)
                .setMessage(R.string.message_need_perm_for_camera)
                .setPositiveButton(R.string.label_ok) { dialogInterface, i ->
                    request.proceed()
                    dialogInterface.dismiss()
                }
                .setNegativeButton(R.string.label_cancel) { dialogInterface, i ->
                    request.cancel()
                    dialogInterface.dismiss()
                }
                .create()

            alert.show()
        }
    }

    @OnPermissionDenied(

        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.MODIFY_AUDIO_SETTINGS
    )
    fun deniedTakePhotoPerm() {
        Log.i(TAG, "deniedTakePhotoPerm")
        showMessageError(R.string.message_error_perm_denied)
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.MODIFY_AUDIO_SETTINGS
    )
    fun onNeverAskAgainCamera() {
        Log.i(TAG, "onNeverAskAgainCamera")
        context?.let {
            val alert = AlertDialog.Builder(it)
                .setMessage(R.string.message_error_perm_denied_before)
                .setPositiveButton(R.string.label_ok) { dialogInterface, i ->
                    presenter.parseSettingsParams()
                    dialogInterface.dismiss()
                }
                .setNegativeButton(R.string.label_cancel) { dialogInterface, i ->
                    dialogInterface.dismiss()
                }
                .create()

            alert.show()
        }
    }

}