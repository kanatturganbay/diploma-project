package thousand.group.onaysurdo.models.global

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HistoryModel(
    var id: Int,
    var client_id: Int,
    var translator_id: Int,
    var duration: String,
    var created_at: String,
    var updated_at: String,
    var status: String,
    var client_video_link: String?,
    var translator_video_link: String?,
    var finished_at: String,
    var started_at: String,
    var text: String,
    var last_pinged_at: String,
    var translator: User?,
    var client: User?
) : Parcelable