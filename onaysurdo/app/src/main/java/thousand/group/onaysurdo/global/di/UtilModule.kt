package thousand.group.onaysurdo.global.di

import android.content.Context
import org.koin.dsl.module
import thousand.group.onaysurdo.global.helpers.MessageStatusHelper
import thousand.group.onaysurdo.global.helpers.ProgressBarHelper
import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.AppSchedulers
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.global.system.SchedulersProvider

val utilModule = module {
    single<SchedulersProvider> { AppSchedulers() }
    single { MessageStatusHelper() }
    single { ProgressBarHelper() }
    single { LocaleStorage }

    factory { (context: Context) ->
        ResourceManager(context)
    }
}