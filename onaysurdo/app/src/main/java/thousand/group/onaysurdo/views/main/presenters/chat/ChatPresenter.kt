package thousand.group.onaysurdo.views.main.presenters.chat

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.arellomobile.mvp.InjectViewState
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.presentation.BasePresenter
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.views.main.interactors.ChatInteractor

@InjectViewState
class ChatPresenter(
    override val context: Context,
    private val resourceManager: ResourceManager,
    private val interactor: ChatInteractor
) : BasePresenter<ChatView>() {

    val webUrl = String.format(
        resourceManager.getString(R.string.format_web_chat),
        interactor.getAccessToken()
    )

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun onStart() {
        viewState.setTitle(R.string.label_chat)
    }

    override fun onFinish() {
    }

    fun parseSettingsParams() {
        val uri = Uri.fromParts(
            resourceManager.getString(R.string.field_package),
            context.packageName,
            null
        )
        val intent = Intent()

        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = uri

        viewState.openSettings(intent)
    }

    fun turnOnChat() {
        if (isInternetActive()) {
            viewState.showUseChatBtn(false)
            viewState.showWebView(true)
            viewState.setWebViewParams()
            viewState.loadWebData(webUrl)
        } else {
            viewState.showMessageError(R.string.message_error_internet_connection)
        }

    }


}
