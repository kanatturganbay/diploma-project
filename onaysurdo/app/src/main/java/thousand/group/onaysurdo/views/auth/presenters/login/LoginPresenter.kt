package thousand.group.onaysurdo.views.auth.presenters.login

import android.content.Context
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.constants.simple.AppConstants
import thousand.group.onaysurdo.global.helpers.ResErrorHelper
import thousand.group.onaysurdo.global.presentation.BasePresenter
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.views.auth.interactors.LoginInteractor

@InjectViewState
class LoginPresenter(
    override val context: Context,
    private val resourceManager: ResourceManager,
    private val interactor: LoginInteractor
) : BasePresenter<LoginView>() {

    companion object {
        internal const val TAG = "LoginPresenter"
    }

    private var iin: String? = null

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun onStart() {
    }

    override fun onFinish() {
    }

    fun iinFilled(
        maskFilled: Boolean,
        extractedValue: String,
        formattedValue: String
    ) {
        if (maskFilled) {
            iin = extractedValue
        } else {
            iin = null
        }
    }

    fun loginBtnClicked() {
        if (isInternetActive()) {
            if (iin != null) {
                interactor.apply {
                    auth(iin!!)
                        .doOnSubscribe { viewState.showProgressBar(true) }
                        .doFinally { viewState.showProgressBar(false) }
                        .subscribe({
                            Log.i(TAG, "Code: ${it.code()}")
                            Log.i(TAG, "Body: ${it.body()}")

                            when (it.code()) {
                                AppConstants.STATUS_CODE_200 -> {
                                    it.body()?.apply {
                                        saveUser(this)
                                        saveUserAccess(true)
                                        saveAccessToken(api_token)
                                        viewState.openMainActivity()
                                    }
                                }
                                else -> {
                                    viewState.showMessageError(
                                        ResErrorHelper.showErrorMessage(
                                            TAG,
                                            it
                                        )
                                    )
                                }
                            }
                        }, {
                            viewState.showMessageError(
                                ResErrorHelper.showThrowableMessage(
                                    resourceManager,
                                    TAG,
                                    it
                                )
                            )
                        }).connect()
                }
            } else {
                viewState.showMessageError(R.string.message_iin_not_filled)
            }
        } else {
            viewState.showMessageError(R.string.message_error_internet_connection)
        }

    }
}