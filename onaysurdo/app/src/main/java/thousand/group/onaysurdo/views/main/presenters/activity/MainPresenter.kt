package thousand.group.onaysurdo.views.main.presenters.activity

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import thousand.group.onaysurdo.global.presentation.BasePresenter
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.views.main.interactors.MainInteractor

@InjectViewState
class MainPresenter(
    override val context: Context,
    private val resourceManager: ResourceManager,
    private val interactor: MainInteractor
) : BasePresenter<MainView>() {

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun onStart() {
        viewState.openChatFragment()
    }

    override fun onFinish() {
    }

    fun signOut() {
        interactor.apply {
            deleteUser()
            setAccessToken("")
            saveUser(false)
            viewState.openAuthActivity()
        }
    }
}