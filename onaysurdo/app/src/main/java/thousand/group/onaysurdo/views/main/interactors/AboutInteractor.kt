package thousand.group.onaysurdo.views.main.interactors

import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.SchedulersProvider
import thousand.group.onaysurdo.views.main.repositories.about.AboutRepository

class AboutInteractor(
    private val repository: AboutRepository,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
)