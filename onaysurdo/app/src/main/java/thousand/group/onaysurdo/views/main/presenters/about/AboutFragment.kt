package thousand.group.onaysurdo.views.main.presenters.about

import android.os.Bundle
import com.arellomobile.mvp.presenter.ProvidePresenter
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.base.BaseFragment
import thousand.group.onaysurdo.global.constants.scopes.MainScope
import thousand.group.onaysurdo.global.helpers.MainFragmentHelper

class AboutFragment : BaseFragment(), AboutView {
    override val layoutRes = R.layout.fragment_about

    companion object {

        const val TAG = "AboutFragment"
        var NAV_TAG = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = TAG,
                statusBarColorRes = R.color.colorPrimaryDark,
                isShowBottomNavBar = true,
                posBottomNav = 2
            )
        )

        fun newInstance(): AboutFragment {
            return AboutFragment()
        }
    }

    @ProvidePresenter
    fun providePresenter(): AboutFragment {
        val scope = getKoin().getOrCreateScope(
            MainScope.ABOUT_SCOPE,
            named(MainScope.ABOUT_SCOPE)
        )
        return scope.get {
            parametersOf(requireActivity())
        }
    }

    override fun initBundle(arguments: Bundle?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initController() {
    }

    override fun onDestroy() {
        getKoin().getScopeOrNull(MainScope.ABOUT_SCOPE)?.close()
        super.onDestroy()
    }
}