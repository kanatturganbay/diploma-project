package thousand.group.onaysurdo.global.constants.simple

object Endpoints {
    internal const val AUTH = "auth"
    internal const val HISTORY_LIST = "client/history"
}