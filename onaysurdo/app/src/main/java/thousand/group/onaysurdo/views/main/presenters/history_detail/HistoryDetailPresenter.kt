package thousand.group.onaysurdo.views.main.presenters.history_detail

import android.content.Context
import android.net.Uri
import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.constants.simple.AppConstants
import thousand.group.onaysurdo.global.presentation.BasePresenter
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.models.global.HistoryModel
import thousand.group.onaysurdo.views.main.interactors.HistoryDetailInteractor

@InjectViewState
class HistoryDetailPresenter(
    override val context: Context,
    private val resourceManager: ResourceManager,
    private val interactor: HistoryDetailInteractor
) : BasePresenter<HistoryDetailView>() {

    private var model: HistoryModel? = null

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun onStart() {
        viewState.setTitle(R.string.label_history)
        setParams()

    }

    override fun onFinish() {
    }

    fun parseBundle(args: Bundle?) {
        args?.apply {
            model = getParcelable(AppConstants.BUNDLE_MODEL)
        }
    }

    fun setParams() {
        model?.apply {
            if (translator_video_link != null) {
                viewState.showTransLayout(true)
                viewState.setTransVideo(Uri.parse(translator_video_link))
            } else {
                viewState.showTransLayout(false)
            }

            if (client_video_link != null) {
                viewState.showClientLayout(true)
                viewState.setClientVideo(Uri.parse(client_video_link))
            } else {
                viewState.showClientLayout(false)
            }

            viewState.setVideoDur(
                String.format(
                    resourceManager.getString(R.string.format_cont_amount),
                    duration
                )
            )

            viewState.setVideoStartDate(
                String.format(
                    resourceManager.getString(R.string.format_start_date),
                    started_at
                )
            )

            viewState.setVideoEndDate(
                String.format(
                    resourceManager.getString(R.string.format_end_date),
                    finished_at
                )
            )

            translator?.apply {
                viewState.setTransFullName(
                    String.format(
                        resourceManager.getString(R.string.format_trans_user),
                        "${name} ${surname} ${patronymic}"
                    )
                )

                viewState.setTransPhoneNumber(
                    String.format(
                        resourceManager.getString(R.string.format_phone),
                        "${phone}"
                    )
                )
            }

            client?.apply {
                viewState.setClientFullName(
                    String.format(
                        resourceManager.getString(R.string.format_client_user),
                        "${name} ${surname} ${patronymic}"
                    )
                )

                viewState.setClientPhoneNumber(
                    String.format(
                        resourceManager.getString(R.string.format_phone),
                        "${phone}"
                    )
                )
            }

        }

        viewState.showSwipe(false)
    }

    fun refresh() {

    }
}