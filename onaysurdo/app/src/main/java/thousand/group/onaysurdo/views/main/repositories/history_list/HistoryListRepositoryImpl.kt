package thousand.group.onaysurdo.views.main.repositories.history_list

import io.reactivex.Single
import retrofit2.Response
import thousand.group.onaysurdo.global.services.ServerService
import thousand.group.onaysurdo.models.global.HistoryModel

class HistoryListRepositoryImpl(
    private val service: ServerService
) : HistoryListRepository {
    override fun getHistoryList(order: String): Single<Response<MutableList<HistoryModel>>> =
        service.getHistoryList(order)
}