package thousand.group.onaysurdo.views.auth.presenters.activity

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import thousand.group.onaysurdo.global.presentation.BasePresenter
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.views.auth.interactors.AuthInteractor

@InjectViewState
class AuthPresenter(
    override val context: Context,
    private val resourceManager: ResourceManager,
    private val interactor: AuthInteractor
) : BasePresenter<AuthView>() {

    override fun onStart() {
        checkAccess()
    }

    override fun onFinish() {
    }

    override fun internetSuccess() {

    }

    override fun internetError() {

    }

    fun checkAccess() {
        if (interactor.isUserSaved()) {
            viewState.openMainActivity()
        } else {
            viewState.openLoginFragment()
        }
    }
}