package thousand.group.onaysurdo.views.auth.presenters.login

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import thousand.group.onaysurdo.global.base.BaseMvpView

@StateStrategyType(OneExecutionStateStrategy::class)
interface LoginView : BaseMvpView {
    fun openMainActivity()
}