package thousand.group.onaysurdo.views.main.di

import org.koin.dsl.module
import thousand.group.onaysurdo.views.main.interactors.*
import thousand.group.onaysurdo.views.main.repositories.about.AboutRepository
import thousand.group.onaysurdo.views.main.repositories.about.AboutRepositoryImpl
import thousand.group.onaysurdo.views.main.repositories.activity.MainRepository
import thousand.group.onaysurdo.views.main.repositories.activity.MainRepositoryImpl
import thousand.group.onaysurdo.views.main.repositories.chat.ChatRepository
import thousand.group.onaysurdo.views.main.repositories.chat.ChatRepositoryImpl
import thousand.group.onaysurdo.views.main.repositories.history_detail.HistoryDetailRepository
import thousand.group.onaysurdo.views.main.repositories.history_detail.HistoryDetailRepositoryImpl
import thousand.group.onaysurdo.views.main.repositories.history_list.HistoryListRepository
import thousand.group.onaysurdo.views.main.repositories.history_list.HistoryListRepositoryImpl

val interactorMainModule = module {
    single<MainRepository> {
        MainRepositoryImpl(get())
    }
    single { MainInteractor(get(), get(), get()) }

    single<ChatRepository> {
        ChatRepositoryImpl(get())
    }
    single { ChatInteractor(get(), get(), get()) }

    single<HistoryListRepository> {
        HistoryListRepositoryImpl(get())
    }
    single { HistoryListInteractor(get(), get(), get()) }

    single<HistoryDetailRepository> {
        HistoryDetailRepositoryImpl(get())
    }
    single { HistoryDetailInteractor(get(), get(), get()) }

    single<AboutRepository> {
        AboutRepositoryImpl(get())
    }
    single { AboutInteractor(get(), get(), get()) }
}