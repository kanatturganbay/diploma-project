package thousand.group.onaysurdo.views.main.presenters.activity

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import thousand.group.onaysurdo.global.base.BaseMvpView

@StateStrategyType(OneExecutionStateStrategy::class)
interface MainView : BaseMvpView {
    fun openChatFragment()

    fun openHistoryListFragment()

    fun openAuthActivity()
}