package thousand.group.onaysurdo.views.main.interactors

import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.SchedulersProvider
import thousand.group.onaysurdo.views.main.repositories.activity.MainRepository

class MainInteractor(
    private val repository: MainRepository,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) {
    fun deleteUser() = storage.deleteUserModel()

    fun setAccessToken(token: String) = storage.setAccessToken(token)

    fun saveUser(save: Boolean) = storage.saveUser(save)
}