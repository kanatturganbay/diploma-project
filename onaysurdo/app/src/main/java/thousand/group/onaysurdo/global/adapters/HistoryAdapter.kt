package thousand.group.onaysurdo.global.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_history.view.*
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.models.global.HistoryModel

class HistoryAdapter(
    val context: Context,
    val OnItemClickListener: (position: Int, model: HistoryModel) -> Unit,
    val onEmptyListener: (show: Boolean) -> Unit
) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    private var dataList = mutableListOf<HistoryModel>()
    private var TAG = "HistoryAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position, getItem(position))
    }

    fun setData(dataList: MutableList<HistoryModel>) {
        this.dataList.clear()
        this.dataList.addAll(dataList)
        notifyDataSetChanged()

        onEmptyListener.invoke(this.dataList.isEmpty())
    }

    fun getItem(position: Int): HistoryModel = dataList[position]

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int, model: HistoryModel) {
            itemView.apply {
                model.apply {

                    cont_amount.text = String.format(
                        context.getString(R.string.format_cont_amount),
                        duration
                    )

                    start_date.text = String.format(
                        context.getString(R.string.format_start_date),
                        started_at
                    )

                    end_date.text = String.format(
                        context.getString(R.string.format_end_date),
                        finished_at
                    )

                    trans_user.text = String.format(
                        context.getString(R.string.format_trans_user),
                        translator?.name
                    )

                    client_user.text = String.format(
                        context.getString(R.string.format_client_user),
                        client?.name
                    )

                    Picasso
                        .get()
                        .load(R.drawable.standart_icon)
                        .error(R.drawable.ic_error_crest)
                        .resizeDimen(R.dimen._130sdp, R.dimen._100sdp)
                        .into(client_ic)

                    setOnClickListener {
                        OnItemClickListener.invoke(position, model)
                    }
                }
            }
        }
    }
}