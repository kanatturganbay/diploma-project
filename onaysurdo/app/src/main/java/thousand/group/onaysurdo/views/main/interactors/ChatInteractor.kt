package thousand.group.onaysurdo.views.main.interactors

import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.SchedulersProvider
import thousand.group.onaysurdo.models.global.User
import thousand.group.onaysurdo.views.main.repositories.chat.ChatRepository

class ChatInteractor(
    private val repository: ChatRepository,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) {
    fun getAccessToken(): String = storage.getAccessToken()

    fun getUserModel(): User? = storage.getUserModel()
}