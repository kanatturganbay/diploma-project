package thousand.group.onaysurdo.views.auth.presenters.login

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.base.BaseFragment
import thousand.group.onaysurdo.global.constants.scopes.AuthScope
import thousand.group.onaysurdo.global.helpers.MainFragmentHelper
import thousand.group.onaysurdo.views.main.presenters.activity.MainActivity

class LoginFragment : BaseFragment(), LoginView {
    override val layoutRes = R.layout.fragment_login

    companion object {

        const val TAG = "LoginFragment"
        var NAV_TAG = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = TAG,
                statusBarColorRes = R.color.colorPrimaryDark
            )
        )

        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    @ProvidePresenter
    fun providePresenter(): LoginPresenter {
        val scope = getKoin().getOrCreateScope(
            AuthScope.LOGIN_SCOPE,
            named(AuthScope.LOGIN_SCOPE)
        )
        return scope.get {
            parametersOf(requireActivity())
        }
    }

    override fun initBundle(arguments: Bundle?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initController() {

        iin_edit?.editText?.apply {
            MaskedTextChangedListener.installOn(
                this,
                getString(R.string.format_iin),
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(
                        maskFilled: Boolean,
                        extractedValue: String,
                        formattedValue: String
                    ) {
                        presenter.iinFilled(maskFilled, extractedValue, formattedValue)
                    }
                }
            )
        }

        login_btn.setOnClickListener {
            presenter.loginBtnClicked()
        }
    }

    override fun onDestroy() {
        getKoin().getScopeOrNull(AuthScope.LOGIN_SCOPE)?.close()
        super.onDestroy()
    }

    override fun openMainActivity() {
        activity?.apply {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }


}