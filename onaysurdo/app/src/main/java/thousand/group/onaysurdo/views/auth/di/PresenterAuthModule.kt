package thousand.group.onaysurdo.views.auth.di

import android.content.Context
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.dsl.module
import thousand.group.onaysurdo.global.constants.scopes.AuthScope
import thousand.group.onaysurdo.views.auth.presenters.activity.AuthPresenter
import thousand.group.onaysurdo.views.auth.presenters.login.LoginPresenter

val presenterAuthModule = module {
    scope(named(AuthScope.AUTH_ACTIVITY_SCOPE)) {
        scoped { (context: Context) ->
            AuthPresenter(context, get { parametersOf(context) }, get())
        }
    }

    scope(named(AuthScope.LOGIN_SCOPE)) {
        scoped { (context: Context) ->
            LoginPresenter(context, get { parametersOf(context) }, get())
        }
    }

}