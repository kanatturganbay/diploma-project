package thousand.group.onaysurdo.views.main.di

import android.content.Context
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.dsl.module
import thousand.group.onaysurdo.global.constants.scopes.MainScope
import thousand.group.onaysurdo.views.main.presenters.about.AboutPresenter
import thousand.group.onaysurdo.views.main.presenters.activity.MainPresenter
import thousand.group.onaysurdo.views.main.presenters.chat.ChatPresenter
import thousand.group.onaysurdo.views.main.presenters.history_detail.HistoryDetailPresenter
import thousand.group.onaysurdo.views.main.presenters.history_list.HistoryListPresenter

val presenterMainModule = module {
    scope(named(MainScope.MAIN_SCOPE)) {
        scoped { (context: Context) ->
            MainPresenter(context, get { parametersOf(context) }, get())
        }
    }

    scope(named(MainScope.CHAT_SCOPE)) {
        scoped { (context: Context) ->
            ChatPresenter(context, get { parametersOf(context) }, get())
        }
    }

    scope(named(MainScope.HISTORY_LIST_SCOPE)) {
        scoped { (context: Context) ->
            HistoryListPresenter(context, get { parametersOf(context) }, get())
        }
    }

    scope(named(MainScope.HISTORY_DETAIL_SCOPE)) {
        scoped { (context: Context) ->
            HistoryDetailPresenter(context, get { parametersOf(context) }, get())
        }
    }

    scope(named(MainScope.ABOUT_SCOPE)) {
        scoped { (context: Context) ->
            AboutPresenter(context, get { parametersOf(context) }, get())
        }
    }
}