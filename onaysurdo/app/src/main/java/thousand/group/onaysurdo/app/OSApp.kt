package thousand.group.onaysurdo.app

import android.app.Application
import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import thousand.group.onaysurdo.global.di.appModule

class OSApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@OSApp)
            modules(appModule)
        }

        RxJavaPlugins.setErrorHandler { }

        initPrefs()

    }

    private fun initPrefs() {
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }
}