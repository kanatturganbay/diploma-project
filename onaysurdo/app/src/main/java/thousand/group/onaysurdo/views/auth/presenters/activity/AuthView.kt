package thousand.group.onaysurdo.views.auth.presenters.activity

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import thousand.group.onaysurdo.global.base.BaseMvpView

@StateStrategyType(OneExecutionStateStrategy::class)
interface AuthView : BaseMvpView {
    fun openLoginFragment()

    fun openMainActivity()
}