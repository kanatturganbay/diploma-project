package thousand.group.onaysurdo.global.di

import thousand.group.onaysurdo.views.auth.di.interactorAuthModule
import thousand.group.onaysurdo.views.auth.di.presenterAuthModule
import thousand.group.onaysurdo.views.main.di.interactorMainModule
import thousand.group.onaysurdo.views.main.di.presenterMainModule

val appModule = listOf(
    networkModule,
    utilModule,
    interactorAuthModule,
    presenterAuthModule,
    interactorMainModule,
    presenterMainModule
)