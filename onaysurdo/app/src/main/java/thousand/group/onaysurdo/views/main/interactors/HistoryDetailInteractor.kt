package thousand.group.onaysurdo.views.main.interactors

import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.SchedulersProvider
import thousand.group.onaysurdo.views.main.repositories.history_detail.HistoryDetailRepository

class HistoryDetailInteractor(
    private val repository: HistoryDetailRepository,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
)