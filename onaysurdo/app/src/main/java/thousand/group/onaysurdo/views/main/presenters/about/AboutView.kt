package thousand.group.onaysurdo.views.main.presenters.about

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import thousand.group.onaysurdo.global.base.BaseMvpView

@StateStrategyType(OneExecutionStateStrategy::class)
interface AboutView : BaseMvpView