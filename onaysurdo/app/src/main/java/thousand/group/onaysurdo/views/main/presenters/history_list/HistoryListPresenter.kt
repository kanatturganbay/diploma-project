package thousand.group.onaysurdo.views.main.presenters.history_list

import android.content.Context
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.constants.simple.AppConstants
import thousand.group.onaysurdo.global.helpers.ResErrorHelper
import thousand.group.onaysurdo.global.presentation.BasePresenter
import thousand.group.onaysurdo.global.system.ResourceManager
import thousand.group.onaysurdo.models.global.HistoryModel
import thousand.group.onaysurdo.views.main.interactors.HistoryListInteractor

@InjectViewState
class HistoryListPresenter(
    override val context: Context,
    private val resourceManager: ResourceManager,
    private val interactor: HistoryListInteractor
) : BasePresenter<HistoryListView>() {

    private var startLoad = false
    private var statusOrder = 1
    private val dimen = resourceManager.getDimension(R.dimen.margin_between_items).toInt()

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun onStart() {
        viewState.setTitle(R.string.label_history)
        viewState.setAdapter()
        getDesc()
    }

    override fun onFinish() {
    }

    fun getDesc() {
        if (statusOrder == 2) {
            startLoad = false
        }
        statusOrder = 1
        getHistoryList("desc")
    }

    fun getAsc() {
        if (statusOrder == 1) {
            startLoad = false
        }
        statusOrder = 2
        getHistoryList("asc")
    }

    fun getHistoryList(order: String) {
        interactor.apply {
            getHistoryList(order)
                .doOnSubscribe {
                    if (!startLoad) {
                        viewState.showProgressBar(true)
                        viewState.showEmptyText(false)
                    }
                }
                .doFinally {
                    viewState.showProgressBar(false)
                    viewState.showSwipe(false)
                }
                .subscribe({
                    Log.i(TAG, "Code: ${it.code()}")
                    Log.i(TAG, "Body: ${it.body()}")

                    when (it.code()) {
                        AppConstants.STATUS_CODE_200 -> {
                            it.body()?.apply {
                                startLoad = true

                                viewState.setList(this)
                                viewState.setDecor(dimen)

                                viewState.showEmptyText(false)
                            }
                        }
                        else -> {
                            viewState.showMessageError(ResErrorHelper.showErrorMessage(TAG, it))

                            showOnError()
                        }
                    }
                }, {
                    viewState.showMessageError(
                        ResErrorHelper.showThrowableMessage(
                            resourceManager,
                            TAG,
                            it
                        )
                    )

                    showOnError()
                })
                .connect()
        }
    }

    fun updateOnResume() {
        viewState.setTitle(R.string.label_history)
        viewState.setAdapter()

        refresh()
    }

    fun refresh() {
        when (statusOrder) {
            1 -> {
                getDesc()
            }
            2 -> {
                getAsc()
            }
        }
    }

    fun spinnerCheckedListener(checked: Boolean) {
        if (checked) {
            getAsc()
        } else {
            getDesc()
        }
    }

    fun openDetailFragment(model: HistoryModel) {
        if (isInternetActive()) {
            viewState.openDetailModel(model)
        } else {
            viewState.showMessageError(R.string.message_error_internet_connection)
        }
    }

    private fun showOnError() {
        if (!startLoad) {
            viewState.showEmptyText(true)
        }
    }

}