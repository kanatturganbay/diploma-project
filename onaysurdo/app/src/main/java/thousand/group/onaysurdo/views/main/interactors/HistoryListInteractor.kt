package thousand.group.onaysurdo.views.main.interactors

import thousand.group.onaysurdo.global.services.storage.LocaleStorage
import thousand.group.onaysurdo.global.system.SchedulersProvider
import thousand.group.onaysurdo.views.main.repositories.history_list.HistoryListRepository

class HistoryListInteractor(
    private val repository: HistoryListRepository,
    private val schedulersProvider: SchedulersProvider,
    private val storage: LocaleStorage
) {
    fun getHistoryList(order: String) =
        repository
            .getHistoryList(order)
            .subscribeOn(schedulersProvider.io())
            .observeOn(schedulersProvider.ui())
}