package thousand.group.onaysurdo.views.main.presenters.history_detail

import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.MediaController
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_history_detail.*
import kotlinx.android.synthetic.main.toolbar_title_backspace.*
import org.koin.android.ext.android.getKoin
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import thousand.group.onaysurdo.R
import thousand.group.onaysurdo.global.base.BaseFragment
import thousand.group.onaysurdo.global.constants.scopes.MainScope
import thousand.group.onaysurdo.global.constants.simple.AppConstants
import thousand.group.onaysurdo.global.extentions.visible
import thousand.group.onaysurdo.global.helpers.MainFragmentHelper
import thousand.group.onaysurdo.models.global.HistoryModel

class HistoryDetailFragment : BaseFragment(), HistoryDetailView {
    override val layoutRes = R.layout.fragment_history_detail

    companion object {

        const val TAG = "HistoryDetailFragment"
        var NAV_TAG = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = TAG,
                statusBarColorRes = R.color.colorBlueFauxDenim,
                isShowBottomNavBar = false
            )
        )

        fun newInstance(model: HistoryModel): HistoryDetailFragment {
            val fragment = HistoryDetailFragment()
            val args = Bundle()
            args.putParcelable(AppConstants.BUNDLE_MODEL, model)
            fragment.arguments = args
            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: HistoryDetailPresenter

    @ProvidePresenter
    fun providePresenter(): HistoryDetailPresenter {
        val scope = getKoin().getOrCreateScope(
            MainScope.HISTORY_DETAIL_SCOPE,
            named(MainScope.HISTORY_DETAIL_SCOPE)
        )
        return scope.get {
            parametersOf(requireActivity())
        }
    }


    private val lp = FrameLayout.LayoutParams(
        FrameLayout.LayoutParams.MATCH_PARENT,
        FrameLayout.LayoutParams.WRAP_CONTENT
    )

    override fun initBundle(arguments: Bundle?) = presenter.parseBundle(arguments)

    override fun initView(savedInstanceState: Bundle?) {
        lp.gravity = Gravity.BOTTOM
    }

    override fun initController() {
        back_icon.setOnClickListener {
            activity?.onBackPressed()
        }

        swipe_refresh_detail.setOnRefreshListener { presenter.refresh() }
    }

    override fun onDestroy() {
        getKoin().getScopeOrNull(MainScope.HISTORY_DETAIL_SCOPE)?.close()
        super.onDestroy()
    }

    override fun setTitle(text: Int) = title_main.setText(text)

    override fun setTransVideo(transUri: Uri) {
        val mediaController = MediaController(requireContext())

        mediaController.setAnchorView(translator_video_view)
        mediaController.setMediaPlayer(translator_video_view)

        translator_video_view.setMediaController(mediaController)
        translator_video_view.setVideoURI(transUri)

        mediaController.layoutParams = lp

        (mediaController.parent as ViewGroup).removeView(mediaController)
        translator_frame.addView(mediaController)

        setTransVideoListener()
    }

    override fun setClientVideo(clientUri: Uri) {
        val mediaController = MediaController(requireContext())

        mediaController.setAnchorView(client_video_view)
        mediaController.setMediaPlayer(client_video_view)

        client_video_view.setMediaController(mediaController)
        client_video_view.setVideoURI(clientUri)

        mediaController.layoutParams = lp

        (mediaController.parent as ViewGroup).removeView(mediaController)
        client_frame.addView(mediaController)

        setClientVideoListener()
    }

    override fun setVideoDur(text: String) {
        video_dur.text = text
    }

    override fun setVideoStartDate(text: String) {
        video_start.text = text
    }

    override fun setVideoEndDate(text: String) {
        video_end.text = text
    }

    override fun setTransFullName(text: String) {
        trans_fullname.text = text
    }

    override fun setTransPhoneNumber(text: String) {
        trans_phone.text = text
    }

    override fun setClientFullName(text: String) {
        client_fullname.text = text
    }

    override fun setClientPhoneNumber(text: String) {
        client_phone.text = text
    }

    override fun showSwipe(show: Boolean) {
        swipe_refresh_detail.isRefreshing = show
    }

    override fun showTransLayout(show: Boolean) {
        translator_text.visible(show)
        translator_frame.visible(show)
    }

    override fun showClientLayout(show: Boolean) {
        client_text.visible(show)
        client_frame.visible(show)
    }

    private fun setTransVideoListener() {
        translator_video_view.setOnPreparedListener {
            try {
                it.setVolume(0F, 0F)
                it.isLooping = true
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        translator_video_view.start()
    }

    private fun setClientVideoListener() {
        client_video_view.setOnPreparedListener {
            try {
                it.setVolume(0F, 0F)
                it.isLooping = true
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        client_video_view.start()
    }

}